# calc.dev

calc.dev is a graphing calculator implemented as a Progressive Web App, designed
to achieve the following goals:

- Accessible
- Seamless experience across devices
- Functional/Utilitarian/Portable (i.e. graphs should be downloadable, data
sets should be importable)

## Getting Started

TODO: Add link to wiki, which will be resposible for teaching users how to use
calc.io
TODO: Possibly create grammar to explain valid expressions

## Contributing

Before contributing any code, always check the Issues and Merge Requests to make
sure you aren't duplicating work/ideas. If you don't see a copy of your Issue,
then please refer to the [Contributing guidelines](https://gitlab.com/z5labs/calc.dev/blob/master/.gitlab/contributing.md)
on how to contribute to calc.io.
