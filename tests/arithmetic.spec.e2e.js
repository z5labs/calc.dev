let testCases = [
  {
    name: "should be able to add natural numbers",
    inputs: ["one", "add", "one"],
    ex: "2"
  },
  {
    name: "should be able to multiply numbers",
    inputs: ["two", "mult", "two"],
    ex: "4"
  },
  {
    name: "should be able to subtract numbers",
    inputs: ["two", "sub", "two"],
    ex: "0"
  },
  {
    name: "should be able to divide numbers",
    inputs: ["two", "div", "two"],
    ex: "1"
  },
  {
    name: "should handle division by zero",
    inputs: ["zero", "div", "zero"],
    ex: "NaN"
  },
  {
    name: "should follow p(e)mdas",
    inputs: ["one", "add", "two", "mult", "two", "sub", "four", "div", "two"],
    ex: "3"
  },
  {
    name: "should handle multi digit numbers",
    inputs: ["one", "zero", "add", "one"],
    ex: "11"
  },
  {
    name: "should handle decimal values",
    inputs: ["one", "decimal", "zero", "add", "zero", "decimal", "one"],
    ex: "1.1"
  }
];

describe("Arithmetic Operations", () => {
  beforeEach(() => cy.visit("/"));

  testCases.forEach(testCase =>
    it(testCase.name, () => {
      testCase.inputs.forEach(i => cy.get(`[data-text="${i}"]`).click());

      cy.get('[data-text="equal"]').click();

      cy.get('[data-text="output"]').should("have.text", testCase.ex);
    })
  );
});
