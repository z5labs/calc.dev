// https://docs.cypress.io/guides/guides/plugins-guide.html

// if you need a custom webpack configuration you can uncomment the following import
// and then use the `file:preprocessor` event
// as explained in the cypress docs
// https://docs.cypress.io/api/plugins/preprocessors-api.html#Examples

// const webpack = require('@cypress/webpack-preprocessor');

module.exports = (on, config) => {
  // on('file:preprocessor', webpack({
  //   webpackOptions: require('@vue/cli-service/webpack.config')
  // }));

  return Object.assign({}, config, {
    fixturesFolder: "tests/cypress/fixtures",
    integrationFolder: "tests",
    screenshotsFolder: "tests/cypress/screenshots",
    videosFolder: "tests/cypress/videos",
    supportFile: "tests/cypress/support/index.js"
  });
};
