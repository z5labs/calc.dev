let swi = "switch";

let trigTestCases = [
  {
    name: "sin",
    inputs: [swi, "sin", swi, "nine", "zero", "rparen"],
    ex: "1"
  },
  {
    name: "cos",
    inputs: [swi, "cos", swi, "zero", "rparen"],
    ex: "1"
  },
  {
    name: "tan",
    inputs: [swi, "tan", swi, "four", "five", "rparen"],
    ex: "1"
  },
  {
    name: "tan(90)",
    inputs: [swi, "tan", swi, "nine", "zero", "rparen"],
    ex: "Infinity"
  }
];

describe("Trigonometric Functions", () => {
  beforeEach(() => cy.visit("/"));

  trigTestCases.forEach(testCase =>
    it(testCase.name, () => {
      testCase.inputs.forEach(v => cy.get(`[data-text="${v}"]`).click());

      cy.get('[data-text="equal"]').click();

      cy.get('[data-text="output"]').should("have.text", testCase.ex);
    })
  );
});

let expTestCases = [
  {
    name: "sqrt",
    inputs: [swi, "sqrt", "four", swi, "rparen"],
    ex: "2"
  },
  {
    name: "^",
    inputs: [swi, "two", "exp", swi, "three"],
    ex: "8"
  }
];

describe("Exponential Functions", () => {
  beforeEach(() => cy.visit("/"));

  expTestCases.forEach(testCase =>
    it(testCase.name, () => {
      testCase.inputs.forEach(v => cy.get(`[data-text="${v}"]`).click());

      cy.get('[data-text="equal"]').click();

      cy.get('[data-text="output"]').should("have.text", testCase.ex);
    })
  );
});
