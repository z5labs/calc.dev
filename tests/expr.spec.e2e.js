let swi = "switch";

let testCases = [
  {
    name: "should handle binary expr",
    inputs: ["one", "add", "one"],
    ex: "2"
  },
  {
    name: "should handle unary expr",
    inputs: [swi, "sin", swi, "nine", "zero", "rparen"],
    ex: "1"
  },
  {
    name: "should handle compund expr-1",
    inputs: ["one", "add", swi, "sin", swi, "nine", "zero", "rparen"],
    ex: "2"
  },
  {
    name: "should handle compund expr-2",
    inputs: [swi, "sin", swi, "nine", "zero", "rparen", "add", "one"],
    ex: "2"
  },
  {
    name: "unary on parenthesis'",
    inputs: [swi, "sin", swi, "four", "five", "mult", "two", "rparen"],
    ex: "1"
  },
  {
    name: "implicit mult: num + unary",
    inputs: [swi, "two", "sqrt", swi, "four", "rparen"],
    ex: "4"
  },
  {
    name: "implicit mult in denominator",
    inputs: ["one", "div", swi, "two", "sqrt", swi, "four", "rparen"],
    ex: "1"
  },
  {
    name: "implicit mult: const + unary",
    inputs: [swi, "pi", "sin", swi, "nine", "zero", "rparen"],
    ex: "3.141592653589793"
  },
  {
    name: "implicit mult: const + const",
    inputs: [swi, "pi", "i", swi],
    ex: "3.141592653589793i"
  }
];

describe("Expressions", () => {
  beforeEach(() => cy.visit("/"));

  testCases.forEach(testCase =>
    it(testCase.name, () => {
      testCase.inputs.forEach(v => cy.get(`[data-text="${v}"]`).click());

      cy.get('[data-text="equal"]').click();

      cy.get('[data-text="output"]').should("have.text", testCase.ex);
    })
  );
});
