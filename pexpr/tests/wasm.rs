#[cfg(target_arch = "wasm32")]
use pexpr::eval_expr;

#[cfg(target_arch = "wasm32")]
use wasm_bindgen_test::*;

#[cfg(target_arch = "wasm32")]
wasm_bindgen_test_configure!(run_in_browser);

#[cfg(target_arch = "wasm32")]
#[wasm_bindgen_test]
fn basic_arithmetic() {
    let res = eval_expr("(1+1i)", 0.0);

    assert_eq!(res[0], 1.0);
    assert_eq!(res[1], 1.0);
}
