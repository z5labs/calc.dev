use num;
use num::complex::Complex;
use std::f64;

pub trait Stack<T> {
    fn push(&mut self, value: T);

    fn pop(&mut self) -> Option<T>;
}

impl<T: num::Num> Stack<T> for Vec<T> {
    fn push(&mut self, value: T) {
        self.push(value);
    }

    fn pop(&mut self) -> Option<T> {
        self.pop()
    }
}

pub fn add<T: Stack<Complex<f64>>>(stack: &mut T) -> Complex<f64> {
    stack.pop().unwrap() + stack.pop().unwrap()
}

pub fn sub<T: Stack<Complex<f64>>>(stack: &mut T) -> Complex<f64> {
    let b = stack.pop().unwrap();
    let a = stack.pop().unwrap();
    a - b
}

pub fn mult<T: Stack<Complex<f64>>>(stack: &mut T) -> Complex<f64> {
    stack.pop().unwrap() * stack.pop().unwrap()
}

pub fn div<T: Stack<Complex<f64>>>(stack: &mut T) -> Complex<f64> {
    let b = stack.pop().unwrap();
    let a = stack.pop().unwrap();
    a / b
}

pub fn sin<T: Stack<Complex<f64>>>(stack: &mut T) -> Complex<f64> {
    stack.pop().unwrap().sin()
}

pub fn cos<T: Stack<Complex<f64>>>(stack: &mut T) -> Complex<f64> {
    stack.pop().unwrap().cos()
}

pub fn tan<T: Stack<Complex<f64>>>(stack: &mut T) -> Complex<f64> {
    stack.pop().unwrap().tan()
}

pub fn sec<T: Stack<Complex<f64>>>(stack: &mut T) -> Complex<f64> {
    1.0 / stack.pop().unwrap().cos()
}

pub fn cot<T: Stack<Complex<f64>>>(stack: &mut T) -> Complex<f64> {
    let x = stack.pop().unwrap();
    x.cos() / x.sin()
}

pub fn csc<T: Stack<Complex<f64>>>(stack: &mut T) -> Complex<f64> {
    1.0 / stack.pop().unwrap().sin()
}

pub fn sqrt<T: Stack<Complex<f64>>>(stack: &mut T) -> Complex<f64> {
    stack.pop().unwrap().sqrt()
}

pub fn exp<T: Stack<Complex<f64>>>(stack: &mut T) -> Complex<f64> {
    let b = stack.pop().unwrap();
    let a = stack.pop().unwrap();

    a.powc(b)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn float_roundoff() {
        assert_eq!(
            add(&mut vec![Complex::new(1.0, 0.0), Complex::new(0.1, 0.0)]),
            Complex::new(1.1, 0.0)
        );
    }
}
