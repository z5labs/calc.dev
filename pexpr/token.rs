use std::cmp::{Ord, Ordering};
use std::convert::TryInto;
use std::f64::consts;
use std::iter::Peekable;
use std::str::CharIndices;

// Token represents a lexographical token in a mathematical expression.
#[derive(PartialEq, Clone, Copy, Debug)]
pub enum Token<'a> {
    Lparen,            // (
    Rparen,            // )
    Float(f64),        // represents any real number e.g. 2 or 1.1
    Imaginary(f64),    // represents an imaginary number e.g. 1i
    Variable(&'a str), // represents a variable e.g. x + 1
    Addition,          // +
    Subtraction,       // -
    Multiplication,    // *
    Division,          // /
    Exp,               // ^
    Sin,               // sin
    Cos,               // cos
    Tan,               // tan
    Sec,               // sec
    Cot,               // cot
    Csc,               // csc
    Sqrt,              // sqrt
}

pub struct Tokenizer<'a> {
    input: &'a str,
    iter: Peekable<CharIndices<'a>>,
    prev: Token<'a>,
    dec_pos: i32,
    start: usize,
    cur: usize,
}

impl<'a> Tokenizer<'a> {
    pub fn new(input: &'a str) -> Tokenizer<'a> {
        Tokenizer {
            input,
            iter: input.char_indices().peekable(),
            prev: Token::Lparen,
            dec_pos: -1,
            start: 0,
            cur: 0,
        }
    }

    fn scan_number(&mut self) -> Option<Token<'a>> {
        self.cur = loop {
            let t = self.iter.peek();
            if t.is_none() {
                break 0;
            }

            let (i, c) = *t.unwrap();

            if c == '.' {
                if self.dec_pos > -1 {
                    panic!("cannot have two decimal points in a number");
                }

                self.dec_pos = i as i32;
                self.iter.next();
                continue;
            }

            if c.is_digit(10) {
                self.iter.next();
                continue;
            }

            if self.dec_pos > -1 && (i as i32) - self.dec_pos == 1 {
                panic!("must have a digit after decimal");
            }

            // Handle complex
            if c == 'i' {
                self.iter.next();
                continue;
            }

            break i;
        };

        if self.cur == 0 {
            self.cur = self.input.len();
        }
        self.prev = self.input[self.start..self.cur].try_into().unwrap();

        self.dec_pos = -1;
        self.start = self.cur;
        Some(self.prev)
    }

    fn scan_op(&mut self, c: char) -> Option<Token<'a>> {
        let mut s = c;
        loop {
            self.cur += s.len_utf8();
            if !s.is_alphanumeric() {
                break;
            }
            let t = self.iter.peek();
            if t.is_none() {
                break;
            }

            let (_, t1) = *t.unwrap();

            if !t1.is_alphanumeric() {
                break;
            }
            s = t1;
            self.iter.next();
        }

        self.prev = self.input[self.start..self.cur].try_into().unwrap();
        self.start = self.cur;

        Some(self.prev)
    }
}

impl<'a> Iterator for Tokenizer<'a> {
    type Item = Token<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        let t = self.iter.next();

        if t.is_none() {
            return None;
        }

        let (i, c) = t.unwrap();
        if c == ' ' {
            self.cur += 1;
            self.start = self.cur;
            return self.next();
        }
        self.cur = i;

        let is_after = match self.prev {
            Token::Rparen | Token::Float(_) | Token::Imaginary(_) | Token::Variable(_) => true,
            _ => false,
        };

        if c == '.' {
            self.dec_pos = i as i32;
            return self.scan_number();
        }
        if c.is_digit(10) || c == '-' && !is_after {
            return self.scan_number();
        }

        self.scan_op(c)
    }
}

impl<'a> Token<'a> {
    pub fn is_unary(self) -> bool {
        match self {
            Token::Sin
            | Token::Cos
            | Token::Tan
            | Token::Sec
            | Token::Cot
            | Token::Csc
            | Token::Sqrt => true,
            _ => false,
        }
    }
}

impl<'a> From<&'a str> for Token<'a> {
    fn from(s: &'a str) -> Token<'a> {
        match s {
            "(" => Token::Lparen,
            ")" => Token::Rparen,
            "+" => Token::Addition,
            "-" => Token::Subtraction,
            "*" => Token::Multiplication,
            "/" => Token::Division,
            "sin" => Token::Sin,
            "cos" => Token::Cos,
            "tan" => Token::Tan,
            "sec" => Token::Sec,
            "cot" => Token::Cot,
            "csc" => Token::Csc,
            "sqrt" => Token::Sqrt,
            "^" => Token::Exp,
            "π" => Token::Float(consts::PI),
            "e" => Token::Float(consts::E),
            _ => {
                if s.ends_with("i") {
                    return Token::Imaginary(s[..s.len() - 1].parse().unwrap());
                }

                match s.parse() {
                    Ok(f) => Token::Float(f),
                    Err(_) => Token::Variable(s),
                }
            }
        }
    }
}

impl<'a> From<f64> for Token<'a> {
    fn from(f: f64) -> Self {
        Token::Float(f)
    }
}

impl<'a> From<u8> for Token<'a> {
    fn from(b: u8) -> Self {
        match b {
            1 => Token::Lparen,
            2 => Token::Rparen,
            3 => Token::Addition,
            4 => Token::Subtraction,
            5 => Token::Multiplication,
            6 => Token::Division,
            7 => Token::Sin,
            8 => Token::Cos,
            9 => Token::Tan,
            10 => Token::Sec,
            11 => Token::Cot,
            12 => Token::Csc,
            13 => Token::Sqrt,
            14 => Token::Exp,
            _ => Token::Lparen,
        }
    }
}

impl<'a> From<&[u8]> for Token<'a> {
    fn from(b: &[u8]) -> Self {
        match b.try_into() {
            Ok(v) => Token::Float(f64::from_be_bytes(v)),
            Err(_) => panic!(),
        }
    }
}

impl<'a> Eq for Token<'a> {}

impl<'a> Ord for Token<'a> {
    fn cmp(&self, other: &Self) -> Ordering {
        let a: i8 = self.into();
        let b: i8 = other.into();

        a.cmp(&b)
    }
}

impl<'a> PartialOrd for Token<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let a: i8 = self.into();
        let b: i8 = other.into();

        a.partial_cmp(&b)
    }
}

impl<'a> Into<u8> for Token<'a> {
    fn into(self) -> u8 {
        match self {
            Token::Lparen => 1,
            Token::Rparen => 2,
            Token::Addition => 3,
            Token::Subtraction => 4,
            Token::Multiplication => 5,
            Token::Division => 6,
            Token::Sin => 7,
            Token::Cos => 8,
            Token::Tan => 9,
            Token::Sec => 10,
            Token::Cot => 11,
            Token::Csc => 12,
            Token::Sqrt => 13,
            Token::Exp => 14,
            _ => panic!(),
        }
    }
}

impl<'a> Into<usize> for Token<'a> {
    fn into(self) -> usize {
        match self {
            Token::Addition => 0,
            Token::Subtraction => 1,
            Token::Multiplication => 2,
            Token::Division => 3,
            Token::Sin => 4,
            Token::Cos => 5,
            Token::Tan => 6,
            Token::Sec => 7,
            Token::Cot => 8,
            Token::Csc => 9,
            Token::Sqrt => 10,
            Token::Exp => 11,
            _ => panic!(),
        }
    }
}

impl<'a> Into<i8> for &Token<'a> {
    fn into(self) -> i8 {
        match *self {
            Token::Lparen => 0,
            Token::Rparen => 1,
            Token::Float(_) => 2,
            Token::Imaginary(_) => 3,
            Token::Variable(_) => 4,
            Token::Addition => 5,
            Token::Subtraction => 6,
            Token::Multiplication => 7,
            Token::Division => 8,
            Token::Exp => 9,
            Token::Sin => 10,
            Token::Cos => 11,
            Token::Tan => 12,
            Token::Sec => 13,
            Token::Cot => 14,
            Token::Csc => 15,
            Token::Sqrt => 16,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use itertools;

    struct DebugDiff<I: Iterator, J: Iterator>(itertools::Diff<I, J>);
    impl<I: Iterator, J: Iterator> std::fmt::Debug for DebugDiff<I, J> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
            match &self.0 {
                itertools::Diff::FirstMismatch(p, _, _) => {
                    f.write_str(&format!("found mismatch at: {}", p))
                }
                itertools::Diff::Shorter(p, _) => {
                    f.write_str(&format!("iterator j only had {} items", p))
                }
                itertools::Diff::Longer(p, _) => {
                    f.write_str(&format!("iterator i only had {} items", p))
                }
            }
        }
    }

    impl<I: Iterator, J: Iterator> std::cmp::PartialEq for DebugDiff<I, J> {
        fn eq(&self, _: &DebugDiff<I, J>) -> bool {
            unimplemented!()
        }
    }

    #[test]
    fn tokenize_basic_expr() {
        let expr = "(1+1)";

        let ex = vec![
            Token::Lparen,
            Token::Float(1.0),
            Token::Addition,
            Token::Float(1.0),
            Token::Rparen,
        ];
        let out = Tokenizer::new(expr);

        let diff = itertools::diff_with(ex, out, |a, b| a == b);
        assert_eq!(None, diff.map(|d| DebugDiff(d)));
    }

    #[test]
    fn tokenize_negative_number() {
        let expr = "(-1+1)";

        let ex = vec![
            Token::Lparen,
            Token::Float(-1.0),
            Token::Addition,
            Token::Float(1.0),
            Token::Rparen,
        ];
        let out = Tokenizer::new(expr);

        let diff = itertools::diff_with(ex, out, |a, b| a == b);
        assert_eq!(None, diff.map(|d| DebugDiff(d)));
    }

    #[test]
    fn tokenize_negative_number_2() {
        let expr = "(sqrt(1.0)-1)";

        let ex = vec![
            Token::Lparen,
            Token::Sqrt,
            Token::Lparen,
            Token::Float(1.0),
            Token::Rparen,
            Token::Subtraction,
            Token::Float(1.0),
            Token::Rparen,
        ];
        let out = Tokenizer::new(expr);

        let diff = itertools::diff_with(ex, out, |a, b| a == b);
        assert_eq!(None, diff.map(|d| DebugDiff(d)));
    }

    #[test]
    fn tokenize_imaginary_expr_basic() {
        let expr = "(1+1i)";

        let ex = vec![
            Token::Lparen,
            Token::Float(1.0),
            Token::Addition,
            Token::Imaginary(1.0),
            Token::Rparen,
        ];
        let out = Tokenizer::new(expr);

        let diff = itertools::diff_with(ex, out, |a, b| a == b);
        assert_eq!(None, diff.map(|d| DebugDiff(d)));
    }

    #[test]
    fn tokenize_imaginary_expr_complex() {
        let expr = "((1+1i)*(1+1i))";

        let ex = vec![
            Token::Lparen,
            Token::Lparen,
            Token::Float(1.0),
            Token::Addition,
            Token::Imaginary(1.0),
            Token::Rparen,
            Token::Multiplication,
            Token::Lparen,
            Token::Float(1.0),
            Token::Addition,
            Token::Imaginary(1.0),
            Token::Rparen,
            Token::Rparen,
        ];
        let out = Tokenizer::new(expr);

        let diff = itertools::diff_with(ex, out, |a, b| a == b);
        assert_eq!(None, diff.map(|d| DebugDiff(d)));
    }

    #[test]
    fn tokenize_implicit_leading_zero() {
        let expr = "(.1+.2)";

        let ex = vec![
            Token::Lparen,
            Token::Float(0.1),
            Token::Addition,
            Token::Float(0.2),
            Token::Rparen,
        ];
        let out = Tokenizer::new(expr);

        let diff = itertools::diff_with(ex, out, |a, b| a == b);
        assert_eq!(None, diff.map(|d| DebugDiff(d)));
    }

    #[test]
    fn tokenize_expr_with_variable() {
        let expr = "(x+1)";

        let ex = vec![
            Token::Lparen,
            Token::Variable("x"),
            Token::Addition,
            Token::Float(1.0),
            Token::Rparen,
        ];
        let out = Tokenizer::new(expr);

        let diff = itertools::diff_with(ex, out, |a, b| a == b);
        assert_eq!(None, diff.map(|d| DebugDiff(d)));
    }

    #[test]
    fn tokenize_expr_with_trig_and_constant() {
        let expr = "(sin(π/2)+1)";

        let ex = vec![
            Token::Lparen,
            Token::Sin,
            Token::Lparen,
            Token::Float(consts::PI),
            Token::Division,
            Token::Float(2.0),
            Token::Rparen,
            Token::Addition,
            Token::Float(1.0),
            Token::Rparen,
        ];
        let out = Tokenizer::new(expr);

        let diff = itertools::diff_with(ex, out, |a, b| a == b);
        assert_eq!(None, diff.map(|d| DebugDiff(d)));
    }
}
