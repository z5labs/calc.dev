#![feature(test)]

extern crate test;

mod ops;
mod token;

pub use crate::token::{Token, Tokenizer};
use num::complex::Complex;
use std::ops::Index;
use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

// eval_expr exports a function for evaluating
// mathematical expressions, which have been
// encoded to a binary format.
//
#[wasm_bindgen]
pub fn eval_expr(expr: &str, x: &[f64]) -> Box<[f64]> {
    let toks = Polisher::new(Tokenizer::new(expr));

    let z = eval(toks, Complex::new(x[0], x[1]));

    Box::new([z.re, z.im])
}

struct Table<T: ops::Stack<Complex<f64>>>([fn(&mut T) -> Complex<f64>; 12]);

impl<'a, T: ops::Stack<Complex<f64>>> Index<Token<'a>> for Table<T> {
    type Output = fn(&mut T) -> Complex<f64>;

    fn index(&self, index: Token<'a>) -> &Self::Output {
        let i: usize = index.into();

        &self.0[i]
    }
}

const LOOKUP: Table<Vec<Complex<f64>>> = Table([
    ops::add,
    ops::sub,
    ops::mult,
    ops::div,
    ops::sin,
    ops::cos,
    ops::tan,
    ops::sec,
    ops::cot,
    ops::csc,
    ops::sqrt,
    ops::exp,
]);

// eval evaluates a mathematical expression in polish notation.
pub fn eval<'a>(expr: impl Iterator<Item = Token<'a>>, var: Complex<f64>) -> Complex<f64> {
    expr.fold(Vec::with_capacity(4), |mut stack, t| {
        match t {
            Token::Float(c) => stack.push(Complex::new(c, 0.0)),
            Token::Imaginary(c) => stack.push(Complex::new(0.0, c)),
            Token::Variable(_) => stack.push(var),
            _ => {
                let op = LOOKUP[t];
                let res = op(&mut stack);
                stack.push(res);
            }
        };

        stack
    })
    .pop()
    .unwrap()
}

pub struct Polisher<'a, T: Iterator<Item = Token<'a>>> {
    expr: T,
    stack: Vec<Token<'a>>,
    last_op: Token<'a>,
    last_operand: Token<'a>,
}

impl<'a, T: Iterator<Item = Token<'a>>> Polisher<'a, T> {
    pub fn new(expr: T) -> Polisher<'a, T> {
        Polisher {
            expr,
            stack: Vec::with_capacity(10),
            last_op: Token::Lparen,
            last_operand: Token::Lparen, // gauranteed to never have been seen before
        }
    }
}

impl<'a, T: Iterator<Item = Token<'a>>> Iterator for Polisher<'a, T> {
    type Item = Token<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.last_op {
            Token::Lparen => {}
            Token::Rparen => {
                let s = self.stack.pop().unwrap();
                if s == Token::Lparen {
                    self.last_op = Token::Lparen;
                    return self.next();
                }

                return Some(s);
            }
            _ => {
                let s = self.stack.pop().unwrap();
                if s < self.last_op {
                    self.stack.push(s);
                    self.stack.push(self.last_op);
                    self.last_op = Token::Lparen;
                    return self.next();
                }

                return Some(s);
            }
        };

        let t = self.expr.next();
        if t.is_none() {
            return None;
        }

        let tok = t.unwrap();
        match tok {
            Token::Float(_) | Token::Imaginary(_) => Some(tok),
            Token::Variable(_) => {
                self.last_operand = tok;
                Some(tok)
            }
            Token::Lparen => {
                self.last_operand = Token::Lparen;
                self.stack.push(Token::Lparen);
                self.next()
            }
            Token::Rparen => {
                self.last_operand = Token::Rparen;

                let s = self.stack.pop().unwrap();
                if s == Token::Lparen {
                    self.last_op = Token::Lparen;
                    return self.next();
                }

                self.last_op = Token::Rparen;
                Some(s)
            }
            _ => {
                self.last_operand = tok;
                let s = self.stack.pop().unwrap();
                if s < tok {
                    self.stack.push(s);
                    self.stack.push(tok);
                    return self.next();
                }

                self.last_op = tok;
                Some(s)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use itertools;
    use test::Bencher;

    struct DebugDiff<I: Iterator, J: Iterator>(itertools::Diff<I, J>);
    impl<I: Iterator, J: Iterator> std::fmt::Debug for DebugDiff<I, J> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
            match &self.0 {
                itertools::Diff::FirstMismatch(p, _, _) => {
                    f.write_str(&format!("found mismatch at: {}", p))
                }
                itertools::Diff::Shorter(p, _) => {
                    f.write_str(&format!("iterator j only had {} items", p))
                }
                itertools::Diff::Longer(p, _) => {
                    f.write_str(&format!("iterator i only had {} items", p))
                }
            }
        }
    }

    impl<I: Iterator, J: Iterator> std::cmp::PartialEq for DebugDiff<I, J> {
        fn eq(&self, _: &DebugDiff<I, J>) -> bool {
            unimplemented!()
        }
    }

    #[test]
    fn follows_pemdas() {
        // 1 + 2 * 2 - 4 / 2 => 1 2 2 * + 4 2 / - => 3
        let toks = Tokenizer::new("1 2 2 * + 4 2 / -");

        let v = eval(toks, Complex::new(0.0, 0.0));

        assert_eq!(v, Complex::new(3.0, 0.0));
    }

    #[test]
    fn handles_unary_ops() {
        let toks = Tokenizer::new("π 2 / sin");

        let v = eval(toks, Complex::new(0.0, 0.0));

        assert_eq!(v, Complex::new(1.0, 0.0));
    }

    #[test]
    fn basic_complex() {
        let expr = Polisher::new(Tokenizer::new("(1+1i)"));

        let v = eval(expr, Complex::new(0.0, 0.0));

        assert_eq!(v, Complex::new(1.0, 1.0));
    }

    #[test]
    fn complex_multiplication() {
        let expr = Polisher::new(Tokenizer::new("((1+1i)*(1+1i))"));

        let v = eval(expr, Complex::new(0.0, 0.0));

        assert_eq!(v, Complex::new(0.0, 2.0));
    }

    #[test]
    fn eval_expr_small() {
        let v = eval_expr("(1+1+1)", &[0.0, 0.0]);
        assert_eq!(v[0], 3.0);
    }

    #[test]
    fn eval_expr_with_variable() {
        let v = eval_expr("(x+1)", &[1.0, 0.0]);
        assert_eq!(v[0], 2.0);
    }

    #[test]
    fn eval_implicit_decimal_leading_zero() {
        let v = eval_expr("(.1+.1)", &[0.0, 0.0]);
        assert_eq!(v[0], 0.2);
    }

    #[test]
    fn polisher_small() {
        let test_expr = "(1+1)";
        let ex_expr = "1 1 +";

        let toks = Polisher::new(Tokenizer::new(test_expr));
        let ex_toks = Tokenizer::new(ex_expr);

        let diff = itertools::diff_with(ex_toks, toks, |a, b| a == b);
        assert_eq!(None, diff.map(|d| DebugDiff(d)));
    }

    #[test]
    fn polisher_unary_and_binary() {
        let test_expr = "(cos(1)*1)";
        let ex_expr = "1 cos 1 *";

        let toks = Polisher::new(Tokenizer::new(test_expr));
        let ex_toks = Tokenizer::new(ex_expr);

        let diff = itertools::diff_with(ex_toks, toks, |a, b| a == b);
        assert_eq!(None, diff.map(|d| DebugDiff(d)));
    }

    #[test]
    fn polisher_large() {
        let test_expr = "(sin(tan(7.0-((tan(tan(tan(4/76.82)))/tan(sqrt(96.86/56+76.03))))))*sin(sin((4)))-tan(sqrt(cos(1)*30.54)))";
        let ex_expr = "7.0 4 76.82 / tan tan tan 96.86 56 / 76.03 + sqrt tan / - tan sin 4 sin sin * 1 cos 30.54 * sqrt tan -";

        let toks = Polisher::new(Tokenizer::new(test_expr));
        let ex_toks = Tokenizer::new(ex_expr);

        let diff = itertools::diff_with(ex_toks, toks, |a, b| a == b);
        assert_eq!(None, diff.map(|d| DebugDiff(d)));
    }

    #[bench]
    fn bench_polisher_small(b: &mut Bencher) {
        let test_expr = "(1+1)";

        b.iter(|| {
            let toks = Polisher::new(Tokenizer::new(test_expr));

            toks.collect::<Vec<Token>>()
        });
    }

    #[bench]
    fn bench_polisher_large(b: &mut Bencher) {
        let test_expr = "(sin(tan(7.0-((tan(tan(tan(4/76.82)))/tan(sqrt(96.86/56+76.03))))))*sin(sin((4)))-tan(sqrt(cos(1)*30.54)))";

        b.iter(|| {
            let toks = Polisher::new(Tokenizer::new(test_expr));

            toks.collect::<Vec<Token>>()
        });
    }

    #[bench]
    fn bench_eval_expr_small(b: &mut Bencher) {
        let expr = "(1+1)";

        b.iter(|| eval_expr(expr, &[0.0, 0.0]));
    }

    #[bench]
    fn bench_eval_expr_large(b: &mut Bencher) {
        let expr = "(sin(tan(7.0-((tan(tan(tan(4/76.82)))/tan(sqrt(96.86/56+76.03))))))*sin(sin((4)))-tan(sqrt(cos(1)*30.54)))";

        b.iter(|| eval_expr(expr, &[0.0, 0.0]));
    }
}
