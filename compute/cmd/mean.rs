use crate::cobra;

use std::io;
use std::num;

use pflag::FlagSet;

pub fn cmd() -> cobra::Command {
    let mut flags = FlagSet::new("mean");
    flags.bool_p("help", 'h', false, "display help message");

    cobra::Command {
        name: "mean",
        short: "Compute the mean of a given dataset.",
        usage: "mean <data>",
        flags,
        stdin: Box::new(io::stdin()),
        stdout: Box::new(io::stdout()),
        stderr: Box::new(io::stderr()),
        run: |cmd, args| {
            let mut total = 0;
            let sum = args
                .join(",")
                .split(',')
                .try_fold(0.0 as f64, |acc, s| -> Result<f64, num::ParseFloatError> {
                    let x: f64 = s.parse()?;
                    total += 1;
                    Ok(acc + x)
                })
                .unwrap();
            writeln!(cmd.stdout, "{}", sum / total as f64).map_err(|err| cobra::Error::from(err))
        },
        subs: Vec::new(),
    }
}
