mod mean;

use crate::cobra;

use pexpr::{eval, Polisher, Tokenizer};

use std::fmt;
use std::io::{self, BufRead};

use num::complex;
use pflag::FlagSet;

pub fn execute(args: Vec<String>) -> cobra::Result {
    let mut c = root();
    c.add_subcommand(mean::cmd());
    c.execute(args)
}

fn root() -> cobra::Command {
    let mut flags = FlagSet::new("compute");
    flags.bool_p("help", 'h', false, "display help message");
    flags.var_p(
        "var",
        'x',
        Complex::new(0.0, 0.0),
        "provide a value for a variable",
    );

    cobra::Command {
        name: "compute",
        short: "compute is a all-in-one cli for your computing needs.",
        usage: "compute [flags] [expr]",
        flags,
        stdin: Box::new(io::stdin()),
        stdout: Box::new(io::stdout()),
        stderr: Box::new(io::stderr()),
        run: |cmd, args| {
            if args.len() == 0 {
                return Ok(());
            }

            let mut expr: Vec<u8> = Vec::new();
            expr.push(b'(');
            if args[0] == "-" {
                let stdin = std::io::stdin();
                let mut stdin = stdin.lock();

                let buf = stdin.fill_buf()?;
                let length = buf.len();
                expr.extend_from_slice(buf);
                stdin.consume(length);
            } else {
                args.iter()
                    .for_each(|a| expr.extend_from_slice(a.as_bytes()));
            }

            let mut expr = String::from_utf8(expr).unwrap();
            expr = expr.trim().to_string();
            expr.push(')');

            let var = cmd.flags.value_of::<Complex>("var")?;
            let toks = Polisher::new(Tokenizer::new(&expr));
            let z = eval(toks, var.0);
            writeln!(&mut cmd.stdout, "{}", Complex(z)).map_err(|err| cobra::Error::from(err))
        },
        subs: Vec::new(),
    }
}

#[derive(Debug, Default)]
struct Complex(complex::Complex<f64>);

impl Complex {
    fn new(re: f64, im: f64) -> Self {
        Self(complex::Complex::new(re, im))
    }
}

impl std::str::FromStr for Complex {
    type Err = complex::ParseComplexError<std::num::ParseFloatError>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let c = s.parse::<complex::Complex<f64>>()?;
        Ok(Self(c))
    }
}

impl fmt::Display for Complex {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.0.re == 0.0 && self.0.im == 0.0 {
            return write!(f, "0");
        }
        if self.0.re != 0.0 {
            write!(f, "{}", self.0.re)?;
            if self.0.im != 0.0 {
                write!(f, "+")?;
            }
        }
        if self.0.im != 0.0 {
            write!(f, "{}i", self.0.im)?;
        }
        Ok(())
    }
}
