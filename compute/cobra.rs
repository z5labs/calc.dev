use std::error;
use std::fmt;
use std::io;
use std::result;

use pflag::{FlagSet, UnknownFlag};

/// A possible error from a command.
#[derive(Debug)]
pub enum Error {
    IO(io::Error),
    UnknownFlag(UnknownFlag),
    Other(String),
}

impl error::Error for Error {}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Error::IO(e)
    }
}

impl From<String> for Error {
    fn from(s: String) -> Self {
        Error::Other(s)
    }
}

impl From<UnknownFlag> for Error {
    fn from(u: UnknownFlag) -> Self {
        Error::UnknownFlag(u)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::IO(err) => write!(f, "{}", err),
            Error::UnknownFlag(err) => write!(f, "{}", err),
            Error::Other(s) => write!(f, "unexpected error: {}", s),
        }
    }
}

/// Represents the result of a commands execution.
pub type Result = result::Result<(), Error>;

pub struct Command {
    pub name: &'static str,
    pub short: &'static str,
    pub usage: &'static str,
    pub flags: FlagSet,
    pub stdin: Box<dyn io::Read>,
    pub stdout: Box<dyn io::Write>,
    pub stderr: Box<dyn io::Write>,

    pub run: fn(&mut Command, Vec<String>) -> Result,

    pub subs: Vec<Command>,
}

impl Command {
    /// Execute the command and any subcommands present in args.
    pub fn execute(&mut self, args: Vec<String>) -> Result {
        let subs = &mut self.subs;
        let mut sub = 0;
        let mut has_sub = false;
        args.iter().for_each(|arg| {
            if let Ok(i) = subs.binary_search_by_key(arg, |cmd| cmd.name.to_string()) {
                sub = i;
                has_sub = true;
            }
        });
        if has_sub {
            let s = &mut subs[sub];
            return s.execute(args[sub + 1..].to_vec());
        }

        let flags = &mut self.flags;
        if !flags.parsed() {
            flags.parse(args.iter().map(|s| s.as_str()))?
        }

        if *flags.value_of::<bool>("help").unwrap() {
            let f = format!("{}", self);
            return write!(&mut self.stdout, "{}", f).map_err(|err| Error::from(err));
        }
        (self.run)(self, args)
    }

    pub fn add_subcommand(&mut self, cmd: Command) {
        self.subs.push(cmd)
    }
}

impl fmt::Display for Command {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(
            f,
            "{}\n{}\n\nUsage:\n\t{}\n",
            self.name, self.short, self.usage
        )?;
        if self.subs.len() > 0 {
            write!(f, "Commands:\n\t")?;
            self.subs
                .iter()
                .try_for_each(|c| write!(f, "{}\t{}\n", c.name, c.short))?;
            write!(f, "\n")?;
        }
        write!(f, "Flags:\n{}", self.flags)
    }
}
