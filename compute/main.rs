mod cmd;
mod cobra;

use std::env;
use std::process;

fn main() {
    let args: Vec<String> = env::args().skip(1).collect();

    let res = cmd::execute(args);

    if let Err(err) = res {
        println!("{}", err);
        process::exit(1)
    }
}
