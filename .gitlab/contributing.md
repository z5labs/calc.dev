# calc.io Contributing Guide

Hi! I'm really excited that you would like to contribute to calc.io. Before
making your contribution, please make sure to take a look and read through
the following guidelines.

- [Code of Conduct](./code_of_conduct.md)
- [Issue Reporting Guidelines](#issue-reporting-guidelines)
- [Merge Request Guidelines](#pull-request-guidelines)
- [Development Setup](#development-setup)
- [Project Structure](#project-structure)
- [Contributing Tests](#contributing-tests)

## Issue Reporting Guidelines

First, before opening a new issue always check current issues and merge requests
to see if any of them address your issue. If none of the current issues address
your concern, then here are some guidelines to opening an issue:

- Open an issue, to discuss a new feature that you would like added.

- Open an issue, to report a bug you discovered. Please follow the issue template
for reporting a bug.

## Merge Request Guidelines

- Checkout a topic branch from a base branch, e.g. `master`, and merge back
against that branch.

- If adding a new feature:
  * Add accompanying test case.
  * Provide a convincing reason to add this feature. Ideally, you should open a
  suggestion issue first and have it approved before working on it.

- If fixing bug:
  * If you are resolving a special issue, add `(fix #xxxx[,#xxxx])`
  (#xxxx is the issue id) in your PR title for a better release log,
  e.g. `update entities encoding/decoding (fix #3899)`.
  * Provide a detailed description of the bug in the PR. Live demo preferred.
  * Add appropriate test coverage if applicable. You can check the coverage of
  your code addition by running `npm run test:unit -- --coverage`.

- It's OK to have multiple small commits as you work on the Merge Request - Gitlab
can automatically squash them before merging.

- Make sure tests pass!

- Commit messages must follow the [commit message convention](./commit-convention.md)
so that changelogs can be automatically generated.

- No need to worry about code style as long as you have installed the dev
dependencies - modified files are automatically formatted with Prettier on
commit (by invoking [Git Hooks](https://git-scm.com/docs/githooks)).

## Development Setup

To begin you will need [Node.js](https://nodejs.org) **version 11+** and [Rust](https://rust-lang.org) **version 1.40+**.

Before cloning the repo, install Rust using [rustup](https://rustup.rs).

After cloning the repo, run:
```bash
npm install
```

A high level overview of tools used:

- [Prettier](https://prettier.io) for code formatting

- [Jest](https://jestjs.io) for unit testing

- [Cypress](https://cypress.io) for e2e testing

- [Vue.js](https://vuejs.org) as the development framework

- [Typescript](https://typescriptlang.org) as a development language

- [Rust](https://rust-lang.org) as a development language

- [wasm-pack](https://rustwasm.github.io/wasm-pack/) for building [Web Assembly](https://webassembly.org) packages
from Rust

### Scripts

**`npm run build`**

The `build` script builds the entire app for production use.

**`npm run serve`**

The `serve` script builds the app for development and rebuilds on file
changes. This is extremely useful for quick debugging/demonstrating a new
feature.

**`npm run test:unit`**

The `test:unit` script simply call the `jest` binary to run all unit tests in
the project. All [Jest CLI Options](https://jestjs.io/docs/en/cli) can be used.
Some examples:

```bash
# run all tests
$ npm run test:unit

# run a tests for a specific file
$ npm run test:unit -- -t fileName
```

**`npm run test:e2e`**

The `test:e2e` script calls the `cypress` binary to run all e2e tests in the
project. All [Cypress Run CLI Options](https://docs.cypress.io/guides/guides/command-line.html#cypress-run)
can be used. Some examples:

```bash
# run all tests in Chrome
$ npm run test:e2e

# run all tests in headless
$ npm run test:e2e -- --headless

# run single test file
$ npm run test:e2e -- --headless -s testFile.spec.e2e.js

# run tests in another browser
$ npm run test:e2e -- --browser firefox
```

## Project Structure

This repository employs a "psuedo" [monorepo](https://en.wikipedia.org/wiki/Monorepo)
setup to manage app development. Directory layout:

- `src`: The UI of the app.
- `pexpr`: The mathematical expression executor.
- `@calc`: A placeholder for the built wasm packages. All built wasm packages
will outputted here e.g. `@calc/pexpr`.

### Importing Wasm Packages

The wasm packages must be asynchronously imported into the app. For example,

```typescript
let eval_expr: any;

import("@calc/pexpr").then(mod => eval_expr = mod.eval_expr);
```

## Contributing Tests

Unit tests are collocated with the code being tested, in files with the `.spec.ts` extension.
Consult the [Jest docs](https://jestjs.io/docs/en/using-matchers) and existing
test cases for how to write new test specs. Here are some additional guidelines:

- Use the minimal API needed for a test case. For example, if a component only
uses one custom directive, then only provide that single directive.
This limits the test's exposure to changes in unrelated parts and makes it more
stable.

- Try to avoid testing browser specific features. If browser specific features
need to be tested, try testing as an e2e test.
