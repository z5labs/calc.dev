### Version

*Version of calc.io this bug occurs in*

### Steps to reproduce

*Specify steps to reproduce bug*

### What is expected?

*Describe what is expected*

### What is actually happening?

*Describe what is actually happening*
