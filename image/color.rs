pub trait Color: Into<(u32, u32, u32, u32)> + From<(u32, u32, u32, u32)> {
    fn rgba(self) -> (u32, u32, u32, u32) {
        self.into()
    }
}

impl<T> Color for T where T: Into<(u32, u32, u32, u32)> + From<(u32, u32, u32, u32)> {}

/// A palette of colors.
#[derive(Debug, Clone)]
pub struct Palette<T: Color>(Vec<T>);

impl<T: Color + Copy> Palette<T> {
    /// Returns the palette color closest to c in Euclidean R,G,B space.
    pub fn convert(&self, color: T) -> T {
        let (cr, cg, cb, ca) = color.rgba();
        *self
            .0
            .iter()
            .min_by_key(|v| {
                let (vr, vg, vb, va) = v.rgba();
                sq_diff(cr, vr) + sq_diff(cg, vg) + sq_diff(cb, vb) + sq_diff(ca, va)
            })
            .unwrap()
    }
}

fn sq_diff(x: u32, y: u32) -> u32 {
    let d = x - y;
    (d * d) >> 2
}

/// A fully opaque CMYK color, having 8 bits for each of cyan, magenta, yellow and black.
#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct CMYK {
    pub c: u8,
    pub m: u8,
    pub y: u8,
    pub k: u8,
}

impl Into<(u32, u32, u32, u32)> for CMYK {
    fn into(self) -> (u32, u32, u32, u32) {
        let w = 0xffff - (self.k as u32) * 0x101;
        let r = (0xffff - (self.c as u32) * 0x101) * w / 0xffff;
        let g = (0xffff - (self.m as u32) * 0x101) * w / 0xffff;
        let b = (0xffff - (self.y as u32) * 0x101) * w / 0xffff;
        (r, g, b, 0xffff)
    }
}

impl From<(u32, u32, u32, u32)> for CMYK {
    fn from(rgba: (u32, u32, u32, u32)) -> Self {
        // All this conversion may be unnecessary but its copied from Golang image/color package.
        let r = ((rgba.0 >> 8) as u8) as u32;
        let g = ((rgba.1 >> 8) as u8) as u32;
        let b = ((rgba.2 >> 8) as u8) as u32;
        let mut w = r;
        if w < g {
            w = g;
        }
        if w < b {
            w = b;
        }
        if w == 0 {
            return Self {
                c: 0,
                m: 0,
                y: 0,
                k: 0xff,
            };
        }
        let c = (w - r) * 0xff / w;
        let m = (w - g) * 0xff / w;
        let y = (w - b) * 0xff / w;
        Self {
            c: c as u8,
            m: m as u8,
            y: y as u8,
            k: (0xff - w) as u8,
        }
    }
}

/// A traditional 32-bit alpha-premultiplied color,
/// having 8 bits for each of red, green, blue and alpha.
///
/// An alpha-premultiplied color component C has been scaled by alpha (A),
/// so has valid values 0 <= C <= A.
#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct RGBA {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl Into<(u32, u32, u32, u32)> for RGBA {
    fn into(self) -> (u32, u32, u32, u32) {
        let r = self.r as u32;
        let g = self.g as u32;
        let b = self.b as u32;
        let a = self.a as u32;
        (r | (r << 8), g | (g << 8), b | (b << 8), a | (a << 8))
    }
}

impl From<(u32, u32, u32, u32)> for RGBA {
    fn from(rgba: (u32, u32, u32, u32)) -> Self {
        Self {
            r: (rgba.0 >> 8) as u8,
            g: (rgba.1 >> 8) as u8,
            b: (rgba.2 >> 8) as u8,
            a: (rgba.3 >> 8) as u8,
        }
    }
}

/// a 64-bit alpha-premultiplied color,
/// having 16 bits for each of red, green, blue and alpha.
///
/// An alpha-premultiplied color component C has been scaled
/// by alpha (A), so has valid values 0 <= C <= A.
#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct RGBA64 {
    pub r: u16,
    pub g: u16,
    pub b: u16,
    pub a: u16,
}

impl Into<(u32, u32, u32, u32)> for RGBA64 {
    fn into(self) -> (u32, u32, u32, u32) {
        (self.r as u32, self.g as u32, self.b as u32, self.a as u32)
    }
}

impl From<(u32, u32, u32, u32)> for RGBA64 {
    fn from(rgba: (u32, u32, u32, u32)) -> Self {
        Self {
            r: rgba.0 as u16,
            g: rgba.1 as u16,
            b: rgba.2 as u16,
            a: rgba.3 as u16,
        }
    }
}

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct NRGBA {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl Into<(u32, u32, u32, u32)> for NRGBA {
    fn into(self) -> (u32, u32, u32, u32) {
        let mut r = self.r as u32;
        let mut g = self.g as u32;
        let mut b = self.b as u32;
        let mut a = self.a as u32;
        r |= r << 8;
        r *= a;
        r /= 0xff;
        g |= g << 8;
        g *= a;
        g /= 0xff;
        b |= b << 8;
        b *= a;
        b /= 0xff;
        a |= a << 8;
        (r, g, b, a)
    }
}

impl From<(u32, u32, u32, u32)> for NRGBA {
    fn from(rgba: (u32, u32, u32, u32)) -> Self {
        let (mut r, mut g, mut b, a) = rgba;
        if a == 0xffff {
            return Self {
                r: (r >> 8) as u8,
                g: (g >> 8) as u8,
                b: (b >> 8) as u8,
                a: 0xff,
            };
        }
        if a == 0 {
            return Self {
                r: 0,
                g: 0,
                b: 0,
                a: 0,
            };
        }

        // Since Into<(u32, u32, u32, u32)> returns an alpha-premultiplied color, we should have r <= a && g <= a && b <= a.
        r = (r * 0xffff) / a;
        g = (g * 0xffff) / a;
        b = (b * 0xffff) / a;
        Self {
            r: (r >> 8) as u8,
            g: (g >> 8) as u8,
            b: (b >> 8) as u8,
            a: (a >> 8) as u8,
        }
    }
}

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct NRGBA64 {
    pub r: u16,
    pub g: u16,
    pub b: u16,
    pub a: u16,
}

impl Into<(u32, u32, u32, u32)> for NRGBA64 {
    fn into(self) -> (u32, u32, u32, u32) {
        let mut r = self.r as u32;
        let mut g = self.g as u32;
        let mut b = self.b as u32;
        let a = self.a as u32;
        r *= a;
        r /= 0xffff;
        g *= a;
        g /= 0xffff;
        b *= a;
        b /= 0xffff;
        (r, g, b, a)
    }
}

impl From<(u32, u32, u32, u32)> for NRGBA64 {
    fn from(rgba: (u32, u32, u32, u32)) -> Self {
        let (mut r, mut g, mut b, a) = rgba;
        if a == 0xffff {
            return Self {
                r: r as u16,
                g: g as u16,
                b: b as u16,
                a: 0xffff,
            };
        }
        if a == 0 {
            return Self {
                r: 0,
                g: 0,
                b: 0,
                a: 0,
            };
        }

        // Since Into<(u32, u32, u32, u32)> returns an alpha-premultiplied color, we should have r <= a && g <= a && b <= a.
        r = (r * 0xffff) / a;
        g = (g * 0xffff) / a;
        b = (b * 0xffff) / a;
        Self {
            r: r as u16,
            g: g as u16,
            b: b as u16,
            a: a as u16,
        }
    }
}

/// An 8-bit alpha color.
#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct Alpha(pub u8);

impl Into<(u32, u32, u32, u32)> for Alpha {
    fn into(self) -> (u32, u32, u32, u32) {
        let mut a = self.0 as u32;
        a |= a << 8;
        (a, a, a, a)
    }
}

impl From<(u32, u32, u32, u32)> for Alpha {
    fn from(rgba: (u32, u32, u32, u32)) -> Self {
        Self((rgba.3 >> 8) as u8)
    }
}

/// An 16-bit alpha color.
#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct Alpha16(pub u16);

pub const Transparent: Alpha16 = Alpha16(0);
pub const Opaque: Alpha16 = Alpha16(0xffff);

impl Into<(u32, u32, u32, u32)> for Alpha16 {
    fn into(self) -> (u32, u32, u32, u32) {
        let a = self.0 as u32;
        (a, a, a, a)
    }
}

impl From<(u32, u32, u32, u32)> for Alpha16 {
    fn from(rgba: (u32, u32, u32, u32)) -> Self {
        Self(rgba.3 as u16)
    }
}

/// An 8-bit grayscale color.
#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct Gray(pub u8);

impl Into<(u32, u32, u32, u32)> for Gray {
    fn into(self) -> (u32, u32, u32, u32) {
        let mut y = self.0 as u32;
        y |= y << 8;
        (y, y, y, 0xffff)
    }
}

impl From<(u32, u32, u32, u32)> for Gray {
    fn from(rgba: (u32, u32, u32, u32)) -> Self {
        // These coefficients (the fractions 0.299, 0.587 and 0.114) are the same
        // as those given by the JFIF specification and used by func RGBToYCbCr in
        // ycbcr.go.
        //
        // Note that 19595 + 38470 + 7471 equals 65536.
        //
        // The 24 is 16 + 8. The 16 is the same as used in RGBToYCbCr. The 8 is
        // because the return value is 8 bit color, not 16 bit color.
        let y = (19595 * rgba.0 + 38470 * rgba.1 + 7471 * rgba.2 + 1 << 15) >> 24;

        Self(y as u8)
    }
}

/// An 16-bit grayscale color.
#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct Gray16(pub u16);

pub const Black: Gray16 = Gray16(0);
pub const White: Gray16 = Gray16(0xffff);

impl Into<(u32, u32, u32, u32)> for Gray16 {
    fn into(self) -> (u32, u32, u32, u32) {
        let y = self.0 as u32;
        (y, y, y, 0xffff)
    }
}

impl From<(u32, u32, u32, u32)> for Gray16 {
    fn from(rgba: (u32, u32, u32, u32)) -> Self {
        // These coefficients (the fractions 0.299, 0.587 and 0.114) are the same
        // as those given by the JFIF specification and used by func RGBToYCbCr in
        // ycbcr.go.
        //
        // Note that 19595 + 38470 + 7471 equals 65536.
        let y = (19595 * rgba.0 + 38470 * rgba.1 + 7471 * rgba.2 + 1 << 15) >> 16;

        Self(y as u16)
    }
}

/// A fully opaque 24-bit Y'CbCr color, having 8 bits
/// each for one luma and two chroma components.
///
/// JPEG, VP8, the MPEG family and other codecs use
/// this color model. Such codecs often use the terms YUV and Y'CbCr
/// interchangeably, but strictly speaking, the term YUV applies only
/// to analog video signals, and Y' (luma) is Y (luminance) after
/// applying gamma correction.
///
/// Conversion between RGB and Y'CbCr is lossy and there are multiple,
/// slightly different formulae for converting between the two. This
/// package follows the JFIF specification at <https://www.w3.org/Graphics/JPEG/jfif3.pdf>.
#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct YCbCr {
    pub y: u8,
    pub cb: u8,
    pub cr: u8,
}

impl Into<(u32, u32, u32, u32)> for YCbCr {
    fn into(self) -> (u32, u32, u32, u32) {
        let yy1 = (self.y as i32) * 0x10101;
        let cb1 = (self.cb as i32) - 128;
        let cr1 = (self.cr as i32) - 128;

        // The bit twiddling below is equivalent to
        //
        // r := (yy1 + 91881*cr1) >> 8
        // if r < 0 {
        //     r = 0
        // } else if r > 0xff {
        //     r = 0xffff
        // }
        //
        // but uses fewer branches and is faster.
        // The code below to compute g and b uses a similar pattern.
        let mut r = yy1 + 91881 * cr1;
        if (r as u32) & 0xff000000 == 0 {
            r >>= 8;
        } else {
            r = (-1 ^ (r >> 31)) & 0xffff;
        }
        let mut g = yy1 - 22554 * cb1 - 46802 * cr1;
        if (g as u32) & 0xff000000 == 0 {
            g >>= 8;
        } else {
            g = (-1 ^ (g >> 31)) & 0xffff;
        }
        let mut b = yy1 + 116130 * cb1;
        if (b as u32) & 0xff000000 == 0 {
            b >>= 8;
        } else {
            b = (-1 ^ (b >> 31)) & 0xffff;
        }

        (r as u32, g as u32, b as u32, 0xffff)
    }
}

impl From<(u32, u32, u32, u32)> for YCbCr {
    fn from(rgba: (u32, u32, u32, u32)) -> Self {
        // The JFIF specification says:
        //	Y' =  0.2990*R + 0.5870*G + 0.1140*B
        //	Cb = -0.1687*R - 0.3313*G + 0.5000*B + 128
        //	Cr =  0.5000*R - 0.4187*G - 0.0813*B + 128
        // https://www.w3.org/Graphics/JPEG/jfif3.pdf says Y but means Y'.
        let r1 = ((rgba.0 >> 8) as u8) as i32;
        let g1 = ((rgba.1 >> 8) as u8) as i32;
        let b1 = ((rgba.2 >> 8) as u8) as i32;

        // yy is in range [0,0xff].
        //
        // Note that 19595 + 38470 + 7471 equals 65536.
        let yy = (19595 * r1 + 38470 * g1 + 7471 * b1 + 1 << 15) >> 16;

        // The bit twiddling below is equivalent to
        //
        // cb := (-11056*r1 - 21712*g1 + 32768*b1 + 257<<15) >> 16
        // if cb < 0 {
        //     cb = 0
        // } else if cb > 0xff {
        //     cb = ^int32(0)
        // }
        //
        // but uses fewer branches and is faster.
        // Note that the uint8 type conversion in the return
        // statement will convert ^int32(0) to 0xff.
        // The code below to compute cr uses a similar pattern.
        //
        // Note that -11056 - 21712 + 32768 equals 0.
        let mut cb = -11056 * r1 - 21712 * g1 + 32768 * b1 + 257 << 15;
        if (cb as u32) & 0xff000000 == 0 {
            cb >>= 16;
        } else {
            cb = -1 ^ (cb >> 31);
        }

        // Note that 32768 - 27440 - 5328 equals 0.
        let mut cr = 32768 * r1 - 27440 * g1 - 5328 * b1 + 257 << 15;
        if (cr as u32) & 0xff000000 == 0 {
            cr >>= 16;
        } else {
            cr = -1 ^ (cr >> 31);
        }

        Self {
            y: yy as u8,
            cb: cb as u8,
            cr: cr as u8,
        }
    }
}

// generate code for direct conversions between provided color types

macro_rules! from_colors {
    ($c:ty, $($cs:ty),+) => {
        $(
            impl From<$cs> for $c {
                fn from(rgba: $cs) -> Self {
                    Self::from(rgba.rgba())
                }
            }
        )+
    };
}

from_colors!(CMYK, RGBA, RGBA64, NRGBA, NRGBA64, Alpha, Alpha16, Gray, Gray16, YCbCr);
from_colors!(RGBA, RGBA64, NRGBA, NRGBA64, Alpha, Alpha16, Gray, Gray16, YCbCr, CMYK);
from_colors!(RGBA64, NRGBA, NRGBA64, Alpha, Alpha16, Gray, Gray16, YCbCr, CMYK, RGBA);
from_colors!(NRGBA, NRGBA64, Alpha, Alpha16, Gray, Gray16, YCbCr, CMYK, RGBA, RGBA64);
from_colors!(NRGBA64, Alpha, Alpha16, Gray, Gray16, YCbCr, CMYK, RGBA, RGBA64, NRGBA);
from_colors!(Alpha, Alpha16, Gray, Gray16, YCbCr, CMYK, RGBA, RGBA64, NRGBA, NRGBA64);
from_colors!(Alpha16, Gray, Gray16, YCbCr, CMYK, RGBA, RGBA64, NRGBA, NRGBA64, Alpha);
from_colors!(Gray, Gray16, YCbCr, CMYK, RGBA, RGBA64, NRGBA, NRGBA64, Alpha, Alpha16);
from_colors!(Gray16, YCbCr, CMYK, RGBA, RGBA64, NRGBA, NRGBA64, Alpha, Alpha16, Gray);
from_colors!(YCbCr, CMYK, RGBA, RGBA64, NRGBA, NRGBA64, Alpha, Alpha16, Gray, Gray16);

mod tests {
    use super::{Color, Gray, YCbCr, CMYK};
    use test::Bencher;

    // Test that YCbCr colors are a superset of Gray colors.
    #[test]
    fn ycbcr_gray() {
        for i in 0..256 {
            let c0 = YCbCr {
                y: i as u8,
                cb: 0x80,
                cr: 0x80,
            }
            .rgba();
            let c1 = Gray(i as u8).rgba();

            assert!(c0 == c1);
            // if err := eq(c0, c1); err != nil {
            // 	t.Errorf("i=0x%02x:\n%v", i, err);
            // }
        }
    }

    // Test that CMYK colors are a superset of Gray colors.
    #[test]
    fn cmyk_gray() {
        for i in 0..256 {
            assert_eq!(
                CMYK {
                    c: 0x00,
                    m: 0x00,
                    y: 0x00,
                    k: (255 - i) as u8
                }
                .rgba(),
                Gray(i as u8).rgba()
            );
        }
    }

    // RGB does saturating arithmetic.
    // Low, middle, and high values can take
    // different paths through the generated code.
    #[bench]
    fn ycbcr_to_rgba_0(b: &mut Bencher) {
        b.iter(|| {
            let n = test::black_box(0);
            let c = YCbCr { y: n, cb: n, cr: n };
            c.rgba()
        })
    }

    #[bench]
    fn ycbcr_to_rgba_128(b: &mut Bencher) {
        b.iter(|| {
            let n = test::black_box(128);
            let c = YCbCr { y: n, cb: n, cr: n };
            c.rgba()
        })
    }

    #[bench]
    fn ycbcr_to_rgba_255(b: &mut Bencher) {
        b.iter(|| {
            let n = test::black_box(255);
            let c = YCbCr { y: n, cb: n, cr: n };
            c.rgba()
        })
    }
}
