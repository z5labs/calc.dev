//! defines types for working with images.

#![feature(test)]

extern crate test;

pub mod color;
pub mod svg;

use std::ops::{Add, Div, Mul, Sub};

use color::Color;

pub trait Img {
    type Pixel: Color;

    fn bounds(&self) -> Rectangle;

    fn set_pixel(&mut self, idx: usize, px: Self::Pixel);

    fn pixel(&self, idx: usize) -> &Self::Pixel;
}

#[derive(Debug, Clone, PartialEq)]
pub struct Image<Pixel: Color> {
    bounds: Rectangle,
    pixels: Vec<Pixel>,
}

impl<Pixel: Color> Image<Pixel> {
    pub fn new(bounds: Rectangle) -> Self {
        let width = bounds.dx() as usize;
        let height = bounds.dy() as usize;

        let cap = width * height;
        let mut pixels: Vec<Pixel> = Vec::with_capacity(cap);
        let default = color::Gray(0).into();
        pixels.resize_with(cap, || Pixel::from(default));

        Image { bounds, pixels }
    }
}

impl<Pixel: Color> Img for Image<Pixel> {
    type Pixel = Pixel;

    fn bounds(&self) -> Rectangle {
        self.bounds
    }

    fn set_pixel(&mut self, idx: usize, px: Pixel) {
        self.pixels[idx] = px;
    }

    fn pixel(&self, idx: usize) -> &Pixel {
        self.pixels.get(idx).unwrap()
    }
}

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct Point(i64, i64);

impl Point {
    pub fn new(x: i64, y: i64) -> Point {
        Point(x, y)
    }

    pub fn modulo(self, r: Rectangle) -> Point {
        let w = r.dx();
        let h = r.dy();
        let mut p = self - r.min;
        p.0 = p.0 % w;
        if p.0 < 0 {
            p.0 += w;
        }
        p.1 = p.1 % h;
        if p.1 < 0 {
            p.1 += h;
        }
        p + r.min
    }

    pub fn within(self, r: Rectangle) -> bool {
        r.min.0 <= self.0 && self.0 < r.max.0 && r.min.1 <= self.1 && self.1 < r.max.1
    }
}

impl Add<Point> for Point {
    type Output = Point;

    fn add(self, q: Point) -> Self::Output {
        Point(self.0 + q.0, self.1 + q.1)
    }
}

impl Sub<Point> for Point {
    type Output = Point;

    fn sub(self, q: Point) -> Self::Output {
        Point(self.0 - q.0, self.1 - q.1)
    }
}

impl Mul<i64> for Point {
    type Output = Point;

    fn mul(self, rhs: i64) -> Self::Output {
        Point(rhs * self.0, rhs * self.1)
    }
}

impl Div<i64> for Point {
    type Output = Point;

    fn div(self, rhs: i64) -> Self::Output {
        Point(self.0 / rhs, self.1 / rhs)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Rectangle {
    min: Point,
    max: Point,
}

impl Rectangle {
    pub fn new(min: Point, max: Point) -> Rectangle {
        Rectangle { min, max }
    }

    pub fn dx(&self) -> i64 {
        self.max.0 - self.min.0
    }

    pub fn dy(&self) -> i64 {
        self.max.1 - self.min.1
    }

    /// empty reports whether the rectangle contains no points.
    pub fn empty(self) -> bool {
        self.min.0 >= self.max.0 || self.min.1 >= self.max.1
    }

    /// overlaps reports whether r and s have a non-empty intersection.
    pub fn overlaps(self, s: Rectangle) -> bool {
        !self.empty()
            && !s.empty()
            && self.min.0 < s.max.0
            && s.min.0 < self.max.0
            && self.min.1 < s.max.1
            && s.min.1 < self.max.1
    }

    /// within reports whether every point in r is in s.
    pub fn within(self, s: Rectangle) -> bool {
        if self.empty() {
            return true;
        }
        // Note that r.Max is an exclusive bound for r, so that r.In(s)
        // does not require that r.Max.In(s).
        s.min.0 <= self.min.0
            && self.max.0 <= s.max.0
            && s.min.1 <= self.min.1
            && self.max.1 <= s.max.1
    }

    /// canon returns the canonical version of r. The returned rectangle has minimum
    /// and maximum coordinates swapped if necessary so that it is well-formed.
    pub fn canon(self) -> Rectangle {
        let mut min = self.min;
        let mut max = self.max;
        if max.0 < min.0 {
            let i = max.0;
            max.0 = min.0;
            min.0 = i;
        }
        if max.1 < min.1 {
            let i = max.1;
            max.1 = min.1;
            min.1 = i;
        }
        Rectangle { min, max }
    }

    /// union returns the smallest rectangle that contains both r and s.
    pub fn union(self, s: Rectangle) -> Rectangle {
        if self.empty() {
            return s;
        }
        if s.empty() {
            return self;
        }
        let mut min = self.min;
        let mut max = self.max;
        if min.0 > s.min.0 {
            min.0 = s.min.0;
        }
        if min.1 > s.min.1 {
            min.1 = s.min.1;
        }
        if max.0 < s.max.0 {
            max.0 = s.max.0;
        }
        if max.1 < s.max.1 {
            max.1 = s.max.1;
        }
        Rectangle { min, max }
    }

    /// intersect returns the largest rectangle contained by both r and s. If the
    /// two rectangles do not overlap then the zero rectangle will be returned.
    pub fn intersect(self, s: Rectangle) -> Rectangle {
        let mut r = self;

        if r.min.0 < s.min.0 {
            r.min.0 = s.min.0;
        }
        if r.min.1 < s.min.1 {
            r.min.1 = s.min.1;
        }
        if r.max.0 > s.max.0 {
            r.max.0 = s.max.0;
        }
        if r.max.1 > s.max.1 {
            r.max.1 = s.max.1;
        }
        // Letting r0 and s0 be the values of r and s at the time that the method
        // is called, this next line is equivalent to:
        //
        // if max(r0.Min.X, s0.Min.X) >= min(r0.Max.X, s0.Max.X) || likewiseForY { etc }
        if r.empty() {
            return Rectangle {
                min: Point(0, 0),
                max: Point(0, 0),
            };
        }
        r
    }

    /// inset returns the rectangle r inset by n, which may be negative. If either
    /// of r's dimensions is less than 2*n then an empty rectangle near the center
    /// of r will be returned.
    pub fn inset(self, n: i64) -> Rectangle {
        let mut r = self;
        if r.dx() < 2 * n {
            r.min.0 = (r.min.0 + r.max.0) / 2;
            r.max.0 = r.min.0;
        } else {
            r.min.0 += n;
            r.max.0 -= n;
        }
        if r.dy() < 2 * n {
            r.min.1 = (r.min.1 + r.max.1) / 2;
            r.max.1 = r.min.1;
        } else {
            r.min.1 += n;
            r.max.1 -= n;
        }
        r
    }
}

impl Add<Point> for Rectangle {
    type Output = Rectangle;

    /// add returns the rectangle r translated by p.
    fn add(self, p: Point) -> Self::Output {
        Rectangle {
            min: self.min + p,
            max: self.max + p,
        }
    }
}

impl Sub<Point> for Rectangle {
    type Output = Rectangle;

    fn sub(self, p: Point) -> Self::Output {
        Rectangle {
            min: self.min - p,
            max: self.max - p,
        }
    }
}
