//! SVG images

use std::fmt;
use std::io;
use std::str;

use crate::color::RGBA;
use crate::{Img, Rectangle};

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Unit {
    Px(f64),
    Percent(f64),
    Em(f64),
    Rem(f64),
}

impl fmt::Display for Unit {
    fn fmt(&self, f: &'_ mut fmt::Formatter) -> fmt::Result {
        match self {
            Unit::Px(v) => write!(f, "{}px", v),
            Unit::Percent(v) => write!(f, "{}%", v),
            Unit::Em(v) => write!(f, "{}em", v),
            Unit::Rem(v) => write!(f, "{}rem", v),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Color<'a> {
    Named(&'a str),
    RGBA(RGBA),
    None,
}

impl<'a> fmt::Display for Color<'a> {
    fn fmt(&self, f: &'_ mut fmt::Formatter) -> fmt::Result {
        match self {
            Color::Named(n) => write!(f, "{}", n),
            Color::RGBA(v) => write!(f, "rgba({}, {}, {}, {})", v.r, v.g, v.b, v.a),
            Color::None => write!(f, "none"),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum LineCap {
    Butt,
    Round,
    Square,
}

impl fmt::Display for LineCap {
    fn fmt(&self, f: &'_ mut fmt::Formatter) -> fmt::Result {
        match self {
            LineCap::Butt => write!(f, "butt"),
            LineCap::Round => write!(f, "round"),
            LineCap::Square => write!(f, "square"),
        }
    }
}

impl Default for LineCap {
    fn default() -> Self {
        LineCap::Butt
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum LineJoin {
    Arcs,
    Bevel,
    Miter,
    MiterClip,
    Round,
}

impl fmt::Display for LineJoin {
    fn fmt(&self, f: &'_ mut fmt::Formatter) -> fmt::Result {
        match self {
            LineJoin::Arcs => write!(f, "arcs"),
            LineJoin::Bevel => write!(f, "bevel"),
            LineJoin::Miter => write!(f, "miter"),
            LineJoin::MiterClip => write!(f, "miter-clip"),
            LineJoin::Round => write!(f, "round"),
        }
    }
}

impl Default for LineJoin {
    fn default() -> Self {
        LineJoin::Miter
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Stroke<'a> {
    color: Color<'a>,
    width: Unit,
    line_cap: LineCap,
    line_join: LineJoin,
}

impl<'a> fmt::Display for Stroke<'a> {
    fn fmt(&self, f: &'_ mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "stroke=\"{}\" stroke-width=\"{}\" stroke-linecap=\"{}\" stroke-linejoin=\"{}\"",
            self.color, self.width, self.line_cap, self.line_join
        )
    }
}

impl<'a> Stroke<'a> {
    pub fn new(color: Color<'a>) -> Self {
        Self {
            color,
            width: Unit::Px(1.0),
            line_cap: LineCap::default(),
            line_join: LineJoin::default(),
        }
    }

    pub fn width(mut self, w: Unit) -> Self {
        self.width = w;
        self
    }

    pub fn line_cap(mut self, lc: LineCap) -> Self {
        self.line_cap = lc;
        self
    }

    pub fn line_join(mut self, lj: LineJoin) -> Self {
        self.line_join = lj;
        self
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Attr<'a> {
    ViewBox(&'a str, &'a str, &'a str, &'a str),
    Xmlns(&'a str),
    Width(Unit),
    Height(Unit),
    Stroke(Stroke<'a>),
    Fill(Color<'a>),
}

impl<'a> fmt::Display for Attr<'a> {
    fn fmt(&self, f: &'_ mut fmt::Formatter) -> fmt::Result {
        match self {
            Attr::ViewBox(x1, y1, x2, y2) => write!(f, "viewBox=\"{} {} {} {}\"", x1, y1, x2, y2),
            Attr::Xmlns(s) => write!(f, "xmlns=\"{}\"", s),
            Attr::Width(w) => write!(f, "width=\"{}\"", w),
            Attr::Height(h) => write!(f, "height=\"{}\"", h),
            Attr::Stroke(s) => write!(f, "{}", s),
            Attr::Fill(c) => write!(f, "fill=\"{}\"", c),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Attrs<'a> {
    attrs: Vec<Attr<'a>>,
}

impl<'a> Attrs<'a> {
    pub fn new() -> Self {
        Self { attrs: Vec::new() }
    }

    pub fn with_capacity(n: usize) -> Self {
        Self {
            attrs: Vec::with_capacity(n),
        }
    }

    pub fn view_box(mut self, x1: &'a str, y1: &'a str, x2: &'a str, y2: &'a str) -> Self {
        self.attrs.push(Attr::ViewBox(x1, y1, x2, y2));
        self
    }

    pub fn xmlns(mut self, s: &'a str) -> Self {
        self.attrs.push(Attr::Xmlns(s));
        self
    }

    pub fn width(mut self, w: Unit) -> Self {
        self.attrs.push(Attr::Width(w));
        self
    }

    pub fn height(mut self, h: Unit) -> Self {
        self.attrs.push(Attr::Height(h));
        self
    }

    pub fn stroke(mut self, s: Stroke<'a>) -> Self {
        self.attrs.push(Attr::Stroke(s));
        self
    }

    pub fn fill(mut self, c: Color<'a>) -> Self {
        self.attrs.push(Attr::Fill(c));
        self
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum ArcFlag {
    Small,
    Large,
}

impl fmt::Display for ArcFlag {
    fn fmt(&self, f: &'_ mut fmt::Formatter) -> fmt::Result {
        let val = match self {
            ArcFlag::Small => 0,
            ArcFlag::Large => 1,
        };
        write!(f, "{}", val)
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum SweepFlag {
    Clockwise,
    Counterclockwise,
}

impl fmt::Display for SweepFlag {
    fn fmt(&self, f: &'_ mut fmt::Formatter) -> fmt::Result {
        let val = match self {
            SweepFlag::Clockwise => 1,
            SweepFlag::Counterclockwise => 0,
        };
        write!(f, "{}", val)
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
enum Command {
    // Move to
    M(f32, f32),
    m(f32, f32),
    // Line to
    L(f32, f32),
    l(f32, f32),
    // Horizontal
    H(f32),
    h(f32),
    // Vertical
    V(f32),
    v(f32),
    // Cubic Bezier
    C(f32, f32, f32, f32, f32, f32),
    c(f32, f32, f32, f32, f32, f32),
    S(f32, f32, f32, f32),
    s(f32, f32, f32, f32),
    // Quadratic Bezier
    Q(f32, f32, f32, f32),
    q(f32, f32, f32, f32),
    T(f32, f32),
    t(f32, f32),
    // Arcs
    A(f32, f32, f32, ArcFlag, SweepFlag, f32, f32),
    a(f32, f32, f32, ArcFlag, SweepFlag, f32, f32),
    // Close Path
    Z,
    z,
}

impl fmt::Display for Command {
    fn fmt(&self, f: &'_ mut fmt::Formatter) -> fmt::Result {
        match self {
            Command::M(x, y) => write!(f, "M{} {}", x, y),
            Command::m(dx, dy) => write!(f, "m{} {}", dx, dy),
            Command::L(x, y) => write!(f, "L{} {}", x, y),
            Command::l(dx, dy) => write!(f, "l{} {}", dx, dy),
            Command::H(x) => write!(f, "H{}", x),
            Command::h(dx) => write!(f, "h{}", dx),
            Command::V(y) => write!(f, "V{}", y),
            Command::v(dy) => write!(f, "v{}", dy),
            Command::C(x1, y1, x2, y2, x, y) => {
                write!(f, "C{},{} {},{} {},{}", x1, y1, x2, y2, x, y)
            }
            Command::c(dx1, dy1, dx2, dy2, dx, dy) => {
                write!(f, "c{},{} {},{} {},{}", dx1, dy1, dx2, dy2, dx, dy)
            }
            Command::S(x2, y2, x, y) => write!(f, "S{},{} {},{}", x2, y2, x, y),
            Command::s(dx2, dy2, dx, dy) => write!(f, "s{},{} {},{}", dx2, dy2, dx, dy),
            Command::Q(x1, y1, x, y) => write!(f, "Q{},{} {},{}", x1, y1, x, y),
            Command::q(dx1, dy1, dx, dy) => write!(f, "q{},{} {},{}", dx1, dy1, dx, dy),
            Command::T(x, y) => write!(f, "T{} {}", x, y),
            Command::t(dx, dy) => write!(f, "t{} {}", dx, dy),
            Command::A(rx, ry, angle, laf, sweep, x, y) => {
                write!(f, "A{} {} {} {} {} {} {}", rx, ry, angle, laf, sweep, x, y)
            }
            Command::a(rx, ry, angle, laf, sweep, dx, dy) => write!(
                f,
                "a{} {} {} {} {} {} {}",
                rx, ry, angle, laf, sweep, dx, dy
            ),
            Command::Z => write!(f, "Z"),
            Command::z => write!(f, "z"),
        }
    }
}

/// The <path> SVG element is the generic element to define a shape.
/// All the basic shapes can be created with a path element.
#[derive(Debug, Clone, PartialEq)]
pub struct Path<'a> {
    d: Vec<Command>,
    attributes: Attrs<'a>,
}

impl<'a> fmt::Display for Path<'a> {
    fn fmt(&self, f: &'_ mut fmt::Formatter) -> fmt::Result {
        write!(f, "<path stroke=\"black\" d=\"")?;
        let mut l = self.d.len() - 1;
        self.d.iter().enumerate().try_for_each(|(i, c)| {
            if i < l {
                return write!(f, "{} ", c);
            }
            write!(f, "{}", c)
        })?;
        write!(f, "\" ")?;

        l = self.attributes.attrs.len() - 1;
        self.attributes
            .attrs
            .iter()
            .enumerate()
            .try_for_each(|(i, c)| {
                if i < l {
                    return write!(f, "{} ", c);
                }
                write!(f, "{}", c)
            })?;
        write!(f, "/>")
    }
}

impl<'a> Path<'a> {
    pub fn new() -> Self {
        Self {
            d: Vec::new(),
            attributes: Attrs { attrs: Vec::new() },
        }
    }

    pub fn attrs(mut self, attrs: Attrs<'a>) -> Self {
        self.attributes = attrs;
        self
    }

    /// Move the current point to the coordinate x,y.
    pub fn M(mut self, x: f32, y: f32) -> Self {
        self.d.push(Command::M(x, y));
        self
    }

    /// Move the current point by shifting the last known
    /// position of the path by dx along the x-axis and by dy along the y-axis.
    pub fn m(mut self, dx: f32, dy: f32) -> Self {
        self.d.push(Command::m(dx, dy));
        self
    }

    /// Draw a line from the current point to the end point specified by x,y.
    pub fn L(mut self, x: f32, y: f32) -> Self {
        self.d.push(Command::L(x, y));
        self
    }

    /// Draw a line from the current point to the end point, which is the
    /// current point shifted by dx along the x-axis and dy along the y-axis.
    pub fn l(mut self, dx: f32, dy: f32) -> Self {
        self.d.push(Command::l(dx, dy));
        self
    }

    /// Draw a horizontal line from the current point to the end point,
    /// which is specified by the x parameter and the current point's y coordinate.
    pub fn H(mut self, x: f32) -> Self {
        self.d.push(Command::H(x));
        self
    }

    /// Draw a horizontal line from the current point to the end point, which is
    /// specified by the current point shifted by dx along the x-axis and the current
    /// point's y coordinate.
    pub fn h(mut self, dx: f32) -> Self {
        self.d.push(Command::h(dx));
        self
    }

    /// Draw a vertical line from the current point to the end point, which is specified
    /// by the y parameter and the current point's x coordinate.
    pub fn V(mut self, y: f32) -> Self {
        self.d.push(Command::V(y));
        self
    }

    /// Draw a vertical line from the current point to the end point, which is specified
    /// by the current point shifted by dy along the y-axis and the current point's x coordinate.
    pub fn v(mut self, dy: f32) -> Self {
        self.d.push(Command::v(dy));
        self
    }

    /// Draw a cubic Bézier curve from the current point to the end point specified by x,y.
    /// The start control point is specified by x1,y1 and the end control point is specified
    /// by x2,y2.
    pub fn C(mut self, x1: f32, y1: f32, x2: f32, y2: f32, x: f32, y: f32) -> Self {
        self.d.push(Command::C(x1, y1, x2, y2, x, y));
        self
    }

    /// Draw a cubic Bézier curve from the current point to the end point, which is the
    /// current point shifted by dx along the x-axis and dy along the y-axis. The start
    /// control point is the current point (starting point of the curve) shifted by dx1
    /// along the x-axis and dy1 along the y-axis. The end control point is the current
    /// point (starting point of the curve) shifted by dx2 along the x-axis and dy2 along
    /// the y-axis.
    pub fn c(mut self, dx1: f32, dy1: f32, dx2: f32, dy2: f32, dx: f32, dy: f32) -> Self {
        self.d.push(Command::c(dx1, dy1, dx2, dy2, dx, dy));
        self
    }

    /// Draw a smooth cubic Bézier curve from the current point to the end point specified
    /// by x,y. The end control point is specified by x2,y2. The start control point is a
    /// reflection of the end control point of the previous curve command. If the previous
    /// command wasn't a cubic Bézier curve, the start control point is the same as the curve
    /// starting point (current point).
    pub fn S(mut self, x2: f32, y2: f32, x: f32, y: f32) -> Self {
        self.d.push(Command::S(x2, y2, x, y));
        self
    }

    /// Draw a smooth cubic Bézier curve from the current point to the end point, which is
    /// the current point shifted by dx along the x-axis and dy along the y-axis. The end
    /// control point is the current point (starting point of the curve) shifted by dx2 along
    /// the x-axis and dy2 along the y-axis. The start control point is a reflection of the
    /// end control point of the previous curve command. If the previous command wasn't a cubic
    /// Bézier curve, the start control point is the same as the curve starting point (current point).
    pub fn s(mut self, dx2: f32, dy2: f32, dx: f32, dy: f32) -> Self {
        self.d.push(Command::s(dx2, dy2, dx, dy));
        self
    }

    /// Draw a quadratic Bézier curve from the current point to the end point specified by x,y.
    /// The control point is specified by x1,y1.
    pub fn Q(mut self, x1: f32, y1: f32, x: f32, y: f32) -> Self {
        self.d.push(Command::Q(x1, y1, x, y));
        self
    }

    /// Draw a quadratic Bézier curve from the current point to the end point, which is the
    /// current point shifted by dx along the x-axis and dy along the y-axis. The control point
    /// is the current point (starting point of the curve) shifted by dx1 along the x-axis and
    /// dy1 along the y-axis.
    pub fn q(mut self, dx1: f32, dy1: f32, dx: f32, dy: f32) -> Self {
        self.d.push(Command::q(dx1, dy1, dx, dy));
        self
    }

    /// Draw a smooth quadratic Bézier curve from the current point to the end point specified
    /// by x,y. The control point is a reflection of the control point of the previous curve
    /// command. If the previous command wasn't a quadratic Bézier curve, the control point is
    /// the same as the curve starting point (current point).
    pub fn T(mut self, x: f32, y: f32) -> Self {
        self.d.push(Command::T(x, y));
        self
    }

    /// Draw a smooth quadratic Bézier curve from the current point to the end point, which is
    /// the current point shifted by dx along the x-axis and dy along the y-axis. The control
    /// point is a reflection of the control point of the previous curve command. If the
    /// previous command wasn't a quadratic Bézier curve, the control point is the same as the
    /// curve starting point (current point).
    pub fn t(mut self, dx: f32, dy: f32) -> Self {
        self.d.push(Command::t(dx, dy));
        self
    }

    /// Draw an Arc curve from the current point to the coordinate x,y. The center of the ellipse
    /// used to draw the arc is determined automatically based on the other parameters of the
    /// command:
    /// * rx and ry are the two radii of the ellipse;
    /// * angle represents a rotation (in degrees) of the ellipse relative to the x-axis;
    /// * large-arc-flag and sweep-flag allows to chose which arc must be drawn as 4 possible arcs can be drawn out of the other parameters.
    ///     * large-arc-flag allows to chose one of the large arc (1) or small arc (0),
    ///     * sweep-flag allows to chose one of the clockwise turning arc (1) or counterclockwise turning arc (0)
    /// The coordinate x,y becomes the new current point for the next command.
    pub fn A(
        mut self,
        rx: f32,
        ry: f32,
        angle: f32,
        laf: ArcFlag,
        sweep: SweepFlag,
        x: f32,
        y: f32,
    ) -> Self {
        self.d.push(Command::A(rx, ry, angle, laf, sweep, x, y));
        self
    }

    /// Draw an Arc curve from the current point to a point for which coordinates are those of
    /// the current point shifted by dx along the x-axis and dy along the y-axis. The center of
    /// the ellipse used to draw the arc is determined automatically based on the other parameters
    /// of the command:
    /// * rx and ry are the two radii of the ellipse;
    /// * angle represents a rotation (in degrees) of the ellipse relative to the x-axis;
    /// * large-arc-flag and sweep-flag allows to chose which arc must be drawn as 4 possible arcs can be drawn out of the other parameters.
    ///     * large-arc-flag allows a choice of large arc (1) or small arc (0),
    ///     * sweep-flag allows a choice of a clockwise arc (1) or counterclockwise arc (0)
    /// The current point gets its X and Y coordinates shifted by dx and dy for the next command.
    pub fn a(
        mut self,
        rx: f32,
        ry: f32,
        angle: f32,
        laf: ArcFlag,
        sweep: SweepFlag,
        dx: f32,
        dy: f32,
    ) -> Self {
        self.d.push(Command::a(rx, ry, angle, laf, sweep, dx, dy));
        self
    }

    /// Close the current subpath by connecting the last point of the path with its initial point.
    /// If the two points are at different coordinates, a straight line is drawn between those two
    /// points.
    pub fn Z(mut self) -> Self {
        self.d.push(Command::Z);
        self
    }

    /// Close the current subpath by connecting the last point of the path with its initial point.
    /// If the two points are at different coordinates, a straight line is drawn between those two
    /// points.
    pub fn z(mut self) -> Self {
        self.d.push(Command::z);
        self
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct SVG<'a> {
    dims: Rectangle,
    view_box: Rectangle,
    paths: Vec<Path<'a>>,
    attributes: Attrs<'a>,
}

impl<'a> SVG<'a> {
    pub fn new(view_box: Rectangle) -> Self {
        SVG {
            view_box,
            dims: view_box,
            paths: Vec::new(),
            attributes: Attrs { attrs: Vec::new() },
        }
    }

    pub fn attrs(mut self, attrs: Attrs<'a>) -> Self {
        self.attributes = attrs;
        self
    }

    /// Provide an arbitrary path.
    ///
    /// Internally, all *shape* methods (rect, circle, etc.) use path
    /// under the hood to help simplify everything SVG needs to track.
    pub fn path(mut self, path: Path<'a>) -> Self {
        self.paths.push(path);
        self
    }

    pub fn rect(self, x: f32, y: f32, width: f32, height: f32) -> Self {
        self.path(
            Path::new()
                .M(x, y)
                .h(width)
                .v(height)
                .h(-width)
                .v(-height)
                .z(),
        )
    }

    pub fn circle(self, cx: f32, cy: f32, r: f32) -> Self {
        self.path(
            Path::new()
                .M(cx - r, cy)
                .a(
                    r,
                    r,
                    0.0,
                    ArcFlag::Small,
                    SweepFlag::Clockwise,
                    2.0 * r,
                    0.0,
                )
                .a(
                    r,
                    r,
                    0.0,
                    ArcFlag::Small,
                    SweepFlag::Clockwise,
                    -2.0 * r,
                    0.0,
                )
                .z(),
        )
    }

    /// "open" paths
    pub fn polyline(self, points: Vec<(f32, f32)>, attrs: Option<Attrs<'a>>) -> Self {
        let mut def_attrs = Attrs::new().fill(Color::None);
        if let Some(mut ats) = attrs {
            def_attrs.attrs.append(&mut ats.attrs);
        }
        let start = points[0];
        let p = points.into_iter().skip(1).fold(
            Path::new().attrs(def_attrs).M(start.0, start.1),
            |acc, (x, y)| acc.L(x, y),
        );
        self.path(p)
    }

    /// "closed" paths
    pub fn polygon(self, points: Vec<(f32, f32)>) -> Self {
        let start = points[0];
        let p = points
            .into_iter()
            .skip(1)
            .fold(Path::new().M(start.0, start.1), |acc, (x, y)| acc.L(x, y));
        self.path(p.z())
    }

    pub fn encode(&self, w: &mut impl io::Write) -> Result<(), Box<dyn std::error::Error>> {
        let view_box = self.view_box;
        write!(
            w,
            "<svg viewBox=\"{} {} {} {}\" ",
            view_box.min.0, view_box.min.1, view_box.max.0, view_box.max.1
        )?;
        let l = self.attributes.attrs.len() - 1;
        self.attributes
            .attrs
            .iter()
            .enumerate()
            .try_for_each(|(i, c)| {
                if i < l {
                    return write!(w, "{} ", c);
                }
                write!(w, "{}", c)
            })?;
        write!(w, ">")?;
        self.paths.iter().try_for_each(|p| write!(w, "{}", p))?;
        write!(w, "</svg>")?;
        Ok(())
    }

    pub fn decode(_r: &mut impl io::Read) -> Result<Self, ()> {
        Err(())
    }
}

impl<'a> fmt::Display for SVG<'a> {
    fn fmt(&self, f: &'_ mut fmt::Formatter) -> fmt::Result {
        let mut v = Vec::new();
        self.encode(&mut v).expect("svg encoding failed");

        write!(f, "{}", String::from_utf8(v).unwrap())
    }
}

impl<'a> str::FromStr for SVG<'a> {
    type Err = ();

    fn from_str(_s: &str) -> Result<Self, Self::Err> {
        todo!()
    }
}

impl<'a> Img for SVG<'a> {
    type Pixel = RGBA;

    fn bounds(&self) -> Rectangle {
        self.dims
    }

    fn pixel(&self, _idx: usize) -> &Self::Pixel {
        todo!()
    }

    fn set_pixel(&mut self, _: usize, _: Self::Pixel) {
        unimplemented!()
    }
}
