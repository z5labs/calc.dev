module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    "plugin:vue/essential",
    "@vue/typescript", // TODO: Convert to @vue/typescript/recommended
    "@vue/prettier",
  ],
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? ["error", { "allow": ["warn", "error"] }] : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    "vue/no-multiple-template-root": "off"
  },
  parserOptions: {
    parser: "@typescript-eslint/parser",
  },
  overrides: [
    {
      files: ["**/*.{j,t}s?(x)"],
      env: {
        jest: true,
      },
    },
  ],
};
