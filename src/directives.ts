import { ObjectDirective } from "vue";

const scrollElToBottom = (el: any, smooth: any) => {
  if (typeof el.scroll === "function") {
    el.scroll({
      top: el.scrollHeight,
      behavior: smooth ? "smooth" : "instant"
    });
  } else {
    el.scrollTop = el.scrollHeight;
  }
};

export const scrollToBottom: ObjectDirective = {
  created: (el, binding) => {
    let scrolled = false;

    el.addEventListener("scroll", (e: Event) => {
      scrolled = el.scrollTop + el.clientHeight + 1 < el.scrollHeight;
    });

    new MutationObserver(e => {
      const config = binding.value || {};
      const pause = config.always === false && scrolled;
      const addedNodes = e[e.length - 1].addedNodes.length;
      const removedNodes = e[e.length - 1].removedNodes.length;

      if (config.scrollonremoved) {
        if (pause || (addedNodes != 1 && removedNodes != 1)) return;
      } else {
        if (pause || addedNodes != 1) return;
      }

      let smooth = config.smooth;
      const loadingRemoved = !addedNodes && removedNodes === 1;
      if (
        loadingRemoved &&
        config.scrollonremoved &&
        "smoothonremoved" in config
      ) {
        smooth = config.smoothonremoved;
      }
      scrollElToBottom(el, smooth);
    }).observe(el, { childList: true, subtree: true });
  },
  mounted: (el, binding) => {
    const config = binding.value || {};
    scrollElToBottom(el, config.smooth);
  }
};

export const directives = {
  "scroll-to-bottom": scrollToBottom
};
