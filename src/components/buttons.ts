import {
  Token,
  float,
  constant,
  variable,
  unary,
  binary,
  paren
} from "@/postfix";

export const btn = {
  switch: {
    name: "switch",
    text: "CHG"
  },
  i: {
    name: "i",
    tok: constant("i")
  },
  pi: {
    name: "pi",
    tok: constant("π")
  },
  e: {
    name: "e",
    tok: constant("e")
  },
  var: {
    name: "var",
    tok: variable("x")
  },
  answer: {
    name: "answer",
    tok: variable("ans")
  },
  backspace: {
    name: "backspace",
    text: "CLR"
  },
  decimal: {
    name: "decimal",
    text: "."
  },
  eval: {
    name: "equal",
    text: "="
  },
  lparen: {
    name: "lparen",
    tok: paren("(")
  },
  rparen: {
    name: "rparen",
    tok: paren(")")
  },
  add: {
    name: "add",
    tok: binary("+")
  },
  sub: {
    name: "sub",
    tok: binary("-")
  },
  mult: {
    name: "mult",
    tok: binary("*")
  },
  div: {
    name: "div",
    tok: binary("/")
  },
  exp: {
    name: "exp",
    tok: binary("^")
  },
  sqrt: {
    name: "sqrt",
    tok: unary("sqrt")
  },
  sin: {
    name: "sin",
    tok: unary("sin")
  },
  cos: {
    name: "cos",
    tok: unary("cos")
  },
  tan: {
    name: "tan",
    tok: unary("tan")
  },
  zero: {
    name: "zero",
    tok: float("0")
  },
  one: {
    name: "one",
    tok: float("1")
  },
  two: {
    name: "two",
    tok: float("2")
  },
  three: {
    name: "three",
    tok: float("3")
  },
  four: {
    name: "four",
    tok: float("4")
  },
  five: {
    name: "five",
    tok: float("5")
  },
  six: {
    name: "six",
    tok: float("6")
  },
  seven: {
    name: "seven",
    tok: float("7")
  },
  eight: {
    name: "eight",
    tok: float("8")
  },
  nine: {
    name: "nine",
    tok: float("9")
  }
};
