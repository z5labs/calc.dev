export default class SWUpdateEvent {
  private registration: ServiceWorkerRegistration;

  constructor(registration: ServiceWorkerRegistration) {
    this.registration = registration;
  }

  update() {
    return this.registration.update();
  }

  skipWaiting() {
    const worker = this.registration.waiting;
    if (!worker) {
      return Promise.resolve();
    }

    return new Promise((resolve, reject) => {
      const channel = new MessageChannel();

      channel.port1.onmessage = event => {
        if (event.data.error) {
          reject(event.data.error);
        } else {
          resolve(event.data);
        }
      };

      worker.postMessage({ type: "skip-waiting" }, [channel.port2]);
    });
  }
}
