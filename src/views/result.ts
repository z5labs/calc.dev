export type Result<S, E> = Ok<S> | Err<E>;

export type Ok<T> = { type: "ok"; value: T };

export function ok<T>(value: T): Ok<T> {
  return { type: "ok", value: value };
}

export type Err<T> = { type: "error"; value: T };

export function err<T>(e: T): Err<T> {
  return { type: "error", value: e };
}

export function try_fn<F extends (...args: any[]) => any>(
  f: F
): (...params: Parameters<F>) => Result<ReturnType<F>, any> {
  return (...params: Parameters<F>) => {
    try {
      const res = f(...params);
      return ok(res);
    } catch (e) {
      return err(e);
    }
  };
}

export function match<S, E>(
  res: Result<S, E>,
  success: (_: S) => void,
  failure: (_: E) => void
) {
  switch (res.type) {
    case "ok":
      success(res.value);
      break;
    case "error":
      failure(res.value);
      break;
    default:
      throw "unknown result type"; // TODO: Make this an error
  }
}
