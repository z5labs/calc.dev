import { Module, Store } from "vuex";
import { Line } from "@/components/ExprList.vue";

interface BasicState {
  lines: Line[];
}

export default function(store: Store<any>): Module<BasicState, any> {
  return {
    namespaced: true,
    state: {
      lines: []
    },
    mutations: {
      pushLine(state, { input, output }) {
        if (state.lines.length === 10) {
          const id = state.lines[0].id;
          state.lines.shift();
          state.lines.push({ id, input, output });
          return;
        }

        state.lines.push({ id: state.lines.length % 10, input, output });
      }
    }
  };
}
