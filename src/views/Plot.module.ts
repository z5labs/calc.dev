import { Module, Store } from "vuex";

interface Bounds {
  x_min: number;
  x_max: number;
  y_min: number;
  y_max: number;
  z_min?: number;
  z_max?: number;
}

interface PlotState {
  exprs: string[][];
  bounds: Bounds;
  step: number;
}

export default function(store: Store<any>): Module<PlotState, any> {
  return {
    namespaced: true,
    state: {
      exprs: [],
      bounds: {
        x_min: -10,
        x_max: 10,
        y_min: -10,
        y_max: 10
      },
      step: 0.1
    },
    mutations: {
      addExpr(state, expr) {
        state.exprs.push(expr);
      },
      updateExpr(state, { idx, expr }) {
        state.exprs[idx] = expr;
      },
      changeStep(state, step) {
        state.step = step;
      },
      updateBounds(state, bounds) {
        state.bounds = {
          ...state.bounds,
          ...bounds
        };
      }
    }
  };
}
