import { provide, inject } from "vue";

export interface Action {
  text: string;
  cb: () => void;
}

export interface Notification {
  title: string;
  body?: string;
  actions: Action[];
}

export class Notifier {
  subscriptions: Array<(n: Notification) => void>;

  constructor() {
    this.subscriptions = [];
  }

  subscribe(h: (n: Notification) => Promise<void>) {
    this.subscriptions.push(h);
  }

  notify(n: Notification) {
    this.subscriptions.map(s => s(n));
  }
}

const NOTIFIER = Symbol();

export const provideNotifier = () => provide(NOTIFIER, new Notifier());

export const useNotifier = () => {
  return inject(NOTIFIER) as Notifier;
};
