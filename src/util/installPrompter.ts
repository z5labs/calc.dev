import { computed, ref, Ref, onUnmounted } from "vue";

export default function(
  browser: Ref<string>,
  isMobile: Ref<boolean>,
  fallback?: () => void
) {
  const mql = window.matchMedia("(display-mode: standalone)");

  const prompter = ref<any>({});
  const installed = ref(mql.matches);
  const showPrompt = ref(
    !installed.value && (isMobile.value || browser.value === "chrome")
  );
  const installable = computed(
    () => !installed.value && (isMobile.value || browser.value === "chrome")
  );

  const getPrompter = (e: Event) => {
    e.preventDefault();
    prompter.value = e;
  };

  const prompt = () => {
    if (!prompter.value || !("prompt" in prompter.value)) {
      if (!fallback) return;

      fallback();
      return;
    }

    prompter.value.prompt();

    if (prompter.value && browser.value === "chrome") {
      window.removeEventListener("beforeinstallprompt", getPrompter);
    }
  };

  const watchDisplayMode = (e: MediaQueryListEvent) => {
    installed.value = e.matches;
  };

  const dismissPrompt = () => (showPrompt.value = !showPrompt.value);

  if (browser.value === "chrome") {
    window.addEventListener("beforeinstallprompt", getPrompter);
  }

  mql.addListener(watchDisplayMode);

  onUnmounted(() => mql.removeListener(watchDisplayMode));

  return {
    showPrompt,
    installable,
    installed,
    dismissPrompt,
    prompt
  };
}
