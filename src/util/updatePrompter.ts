import { computed, ref, onMounted, onUnmounted } from "vue";
import { bus } from "@/event";
import SWUpdateEvent from "@/SWUpdateEvent";

export default function() {
  const updateEvent = ref<SWUpdateEvent>(null as any);
  const updatable = computed(() => !!updateEvent.value);

  const update = () => {
    if (!updateEvent.value) return;

    updateEvent.value.skipWaiting().then(() => {
      location.reload(true);
    });
    updateEvent.value = null as any;
  };

  function handleUpdateEv(e: SWUpdateEvent | undefined) {
    updateEvent.value = e as SWUpdateEvent;
  }

  onMounted(() => {
    bus.on("sw-updated", handleUpdateEv);
  });

  onUnmounted(() => {
    bus.off("sw-updated", handleUpdateEv);
  });

  return {
    updateAvailable: updatable,
    update
  };
}
