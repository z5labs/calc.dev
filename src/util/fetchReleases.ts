import { reactive, ref, onMounted, watch } from "vue";
import marked from "marked";

// Commit represents a Conventional Commit.
interface Commit {
  hash: string;
  subject: string;
  description: string;
  url: string;
}

// Release represents the most basic info about a Gitlab release.
interface Release {
  name: string;
  timestamp: string;
  features?: Commit[];
  bugFixes?: Commit[];
}

interface ReleaseGql {
  name: string;
  timestamp: string;
  description: string;
}

interface Project {
  releases: Connection<ReleaseGql>;
}

interface Connection<T> {
  edges: Edge<T>[];
  pageInfo: PageInfo;
}

interface Edge<T> {
  node: T;
}

interface PageInfo {
  endCursor: string;
  hasNextPage: boolean;
}

const dtRx = /[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])/;

export default function() {
  const releases = ref<Release[]>([]);

  const pageInfo = reactive({
    cursor: "",
    more: false
  });

  onMounted(() =>
    fetchGQL(
      "https://gitlab.com/api/graphql",
      `query {
          project(fullPath: "z5labs/calc.dev") {
            releases(first: 10) {
              pageInfo {
                endCursor
                hasNextPage
              }
              edges {
                node {
                  name
                  timestamp:releasedAt
                  description
                }
              }
            }
          }
        }`
    )
      .then(resp => resp.json())
      .then(d => d.data.project)
      .then((proj: Project) => {
        pageInfo.cursor = proj.releases.pageInfo.endCursor;
        pageInfo.more = proj.releases.pageInfo.hasNextPage;

        const nodes = proj.releases.edges.map(v => v.node);

        releases.value = nodes.map(n => {
          const dt = dtRx.exec(n.timestamp);

          const toks = marked.lexer(n.description);

          let cur = "features";
          const features: Commit[] = [];
          const bugFixes: Commit[] = [];

          toks.forEach((tok: any) => {
            if ("type" in tok && tok.type === "heading") {
              if (tok.text !== "Features") {
                cur = "fixes";
                return;
              }
              cur = "features";
              return;
            }

            if ("type" in tok && tok.type !== "list") return;

            const itemToks = tok.items.map((i: any) => i.tokens[0].tokens);

            const commits = parseCommits(itemToks);

            if (cur === "features") {
              features.push(...commits);
              return;
            }
            bugFixes.push(...commits);
          });

          return {
            name: n.name,
            timestamp: dt ? dt[0] : n.timestamp,
            features,
            bugFixes
          };
        });
      })
  );

  const getMore = ref(false);

  watch(
    () => getMore.value,
    async v => {
      if (!v || !pageInfo.more) return;

      const resp = await fetchGQL(
        "https://gitlab.com/api/graphql",
        `query {
          project(fullPath: "z5labs/calc.dev") {
            releases(first: 10, after: "${pageInfo.cursor}") {
              pageInfo {
                endCursor
                hasNextPage
              }
              edges {
                node {
                  name
                  timestamp:releasedAt
                  description
                }
              }
            }
          }
        }`
      );

      const d = await resp.json();

      const proj = d.data.project as Project;

      pageInfo.cursor = proj.releases.pageInfo.endCursor;
      pageInfo.more = proj.releases.pageInfo.hasNextPage;

      const nodes = proj.releases.edges.map(v => v.node);

      releases.value.push(
        ...nodes.map(n => {
          const dt = dtRx.exec(n.timestamp);

          const toks = marked.lexer(n.description);

          let cur = "features";
          const features: Commit[] = [];
          const bugFixes: Commit[] = [];

          toks.forEach((tok: any) => {
            if ("type" in tok && tok.type === "heading") {
              if (tok.text !== "Features") {
                cur = "fixes";
                return;
              }
              cur = "features";
              return;
            }

            if ("type" in tok && tok.type !== "list") return;

            const itemToks = tok.items.map((i: any) => i.tokens[0].tokens);

            const commits = parseCommits(itemToks);

            if (cur === "features") {
              features.push(...commits);
              return;
            }
            bugFixes.push(...commits);
          });

          return {
            name: n.name,
            timestamp: dt ? dt[0] : n.timestamp,
            features,
            bugFixes
          };
        })
      );
      getMore.value = false;
    }
  );

  const fetchMore = () => {
    getMore.value = true;
  };

  return {
    releases,
    pageInfo,
    fetchMore
  };
}

function fetchGQL(endpoint: string, query: string) {
  return fetch(endpoint, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ query: query })
  });
}

interface Token {
  type: string;
  text: string;
  raw: string;
  href: string;
  items: Token[];
  tokens: Token[];
}

// getCommits parses a list of Commits from their Markdown representation.
function parseCommits(toks: Token[][]): Commit[] {
  let commits: Commit[] = [];
  toks.forEach(tok => {
    const commit = {
      subject: tok[0].text.slice(0, -1),
      description: tok[1].raw.slice(0, -1),
      url: 2 in tok ? tok[2].href : "",
      hash: 2 in tok ? tok[2].text : ""
    };

    commits.push(commit);
  });

  return commits;
}
