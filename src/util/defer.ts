export function defer(f: Function, ...params: unknown[]): Function {
  return () => f.apply(null, params);
}
