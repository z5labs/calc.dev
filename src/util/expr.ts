import { ref, Ref } from "vue";
import { Units } from "@/store";
import {
  binary,
  constant,
  float,
  paren,
  isConst,
  isFloat,
  isUnary,
  isVar,
  Token
} from "@/postfix";
import { try_fn, Result } from "@/views/result";

const angleConv = [
  paren("("),
  constant("π"),
  binary("/"),
  float("180"),
  paren(")"),
  binary("*")
];
const lparen = paren("(");
const rparen = paren(")");

export default function(units: Ref<Units>) {
  const rewrite = (toks: Token[]): Token[] =>
    toks.reduce((acc: Token[], t: Token, i: number, arr: Token[]) => {
      const tok = t.value !== "i" ? t : constant("1i");

      const prev = i > 0 ? arr[i - 1] : null;
      if (!prev || (isFloat(prev) && isFloat(tok))) {
        acc.push(tok);
        return acc;
      }

      const trig = i > 1 ? arr[i - 2] : null;
      if (
        units.value.angle === "degree" &&
        trig &&
        isUnary(trig) &&
        isTrig(trig.value) &&
        prev.value === "("
      ) {
        acc.push(...angleConv, tok);
        return acc;
      }

      if (
        (prev.value === ")" || isConst(prev) || isFloat(prev) || isVar(prev)) &&
        (tok.value === "(" ||
          isConst(tok) ||
          isFloat(tok) ||
          isVar(tok) ||
          isUnary(tok))
      ) {
        acc.push(binary("*"), tok);
        return acc;
      }
      acc.push(tok);
      return acc;
    }, []);

  const toString = (toks: string[]) => toks.reduce((acc, t) => acc + t, "");

  const evalExpr: Ref<
    ((expr: string[], x: Float64Array) => Result<Float64Array, Error>) | null
  > = ref(null);

  import("@calc/pexpr")
    .then(mod => {
      const eval_expr = try_fn(mod.eval_expr);

      evalExpr.value = (
        expr: string[],
        x: Float64Array
      ): Result<Float64Array, Error> => {
        const toks = ["(", ...expr, ")"];
        const s = toString(toks);
        return eval_expr(s, x);
      };
    })
    .catch(err => console.error(err));

  return {
    evalExpr,
    rewrite,
    toString
  };
}

function isTrig(s: string): boolean {
  return (
    s === "cos" ||
    s === "sin" ||
    s === "tan" ||
    s === "sec" ||
    s === "csc" ||
    s === "cot"
  );
}
