import { reactive, ref, computed } from "vue";
import { Token } from "@/postfix";

export interface Character {
  mod?: Modifier[];
  name: string;
  icon?: string;
  iconAlt?: string;
}

export enum Modifier {
  Alt,
  Ctrl,
  Meta,
  Shift
}

export interface CharMap {
  [char: string]: Character;
}

export const CHARS: CharMap = {
  switch: { name: "switch" },
  backspace: { name: "backspace" },
  "=": { name: "=" },
  "(": { name: "(" },
  ")": { name: ")" },
  ".": { name: "." },
  ",": { name: "," },
  ans: { name: "ans" },
  "0": { name: "0" },
  "1": { name: "1" },
  "2": { name: "2" },
  "3": { name: "3" },
  "4": { name: "4" },
  "5": { name: "5" },
  "6": { name: "6" },
  "7": { name: "7" },
  "8": { name: "8" },
  "9": { name: "9" },
  e: { name: "e" },
  i: { name: "i" },
  π: { name: "π" },
  x: { name: "x" },
  "+": { name: "+" },
  "-": { name: "-" },
  "*": { name: "*" },
  "/": { name: "/" },
  "^": { name: "^" },
  tan: { name: "tan" },
  sin: { name: "sin" },
  cos: { name: "cos" },
  sqrt: { name: "sqrt" }
};

export interface Key {
  size?: number;
  direction?: "row" | "column";
  char: Character;
  ariaLabel?: string;
}

export function key(
  char: Character,
  size = 1,
  direction = "row",
  ariaLabel?: string
): Key {
  return {
    size: size,
    direction: direction as "row" | "column",
    char: char,
    ariaLabel: ariaLabel
  };
}

export function labeledKey(char: Character, ariaLabel: string): Key {
  return key(char, undefined, undefined, ariaLabel);
}

export interface Layout {
  baseUnit: number;
  rows: number;
  columns: number;
  keys: Key[];
}

interface LayoutMap {
  [name: string]: Layout;
}

export interface KeyMap {
  [name: string]: Key;
}

export const KEYS: KeyMap = {
  switch_layout: labeledKey(CHARS["switch"], "change layout"),
  lparen: labeledKey(CHARS["("], "left parenthesis"),
  rparen: labeledKey(CHARS[")"], "right parenthesis"),

  // Unary Operators
  tangent: labeledKey(CHARS["tan"], "tangent"),
  sine: labeledKey(CHARS["sin"], "sine"),
  cosine: labeledKey(CHARS["cos"], "cosine"),
  sqrt: labeledKey(CHARS["sqrt"], "square root"),

  // Binary Operators
  division: labeledKey(CHARS["/"], "divide"),
  multiplication: labeledKey(CHARS["*"], "multiply"),
  subtraction: labeledKey(CHARS["-"], "subtract"),
  addition: labeledKey(CHARS["+"], "add"),
  exponent: labeledKey(CHARS["^"], "exponent"),

  backspace: labeledKey(CHARS["backspace"], "backspace"),
  decimal: labeledKey(CHARS["."], "decimal"),
  comma: labeledKey(CHARS[","], "comma"),
  equal: labeledKey(CHARS["="], "enter"),

  // 0-9
  zero: labeledKey(CHARS["0"], "zero"),
  one: labeledKey(CHARS["1"], "one"),
  two: labeledKey(CHARS["2"], "two"),
  three: labeledKey(CHARS["3"], "three"),
  four: labeledKey(CHARS["4"], "four"),
  five: labeledKey(CHARS["5"], "five"),
  six: labeledKey(CHARS["6"], "six"),
  seven: labeledKey(CHARS["7"], "seven"),
  eight: labeledKey(CHARS["8"], "eight"),
  nine: labeledKey(CHARS["9"], "nine"),

  // Constants
  answer: labeledKey(CHARS["ans"], "answer"),
  pi: labeledKey(CHARS["π"], "pi"),
  i: labeledKey(CHARS["i"], "i"),
  e: labeledKey(CHARS["e"], "e"),

  // Variables
  x: labeledKey(CHARS["x"], "variable")
};

const stdLayouts: LayoutMap = {
  arithmetic: {
    baseUnit: 1,
    rows: 5,
    columns: 4,
    keys: [
      KEYS.switch_layout,
      KEYS.lparen,
      KEYS.rparen,
      KEYS.division,
      KEYS.seven,
      KEYS.eight,
      KEYS.nine,
      KEYS.multiplication,
      KEYS.four,
      KEYS.five,
      KEYS.six,
      KEYS.subtraction,
      KEYS.one,
      KEYS.two,
      KEYS.three,
      KEYS.addition,
      KEYS.backspace,
      KEYS.zero,
      KEYS.decimal,
      KEYS.equal
    ]
  },
  transcendental: {
    baseUnit: 1,
    rows: 5,
    columns: 4,
    keys: [
      KEYS.switch_layout,
      KEYS.answer,
      KEYS.e,
      KEYS.exponent,
      KEYS.seven,
      KEYS.eight,
      KEYS.nine,
      KEYS.sine,
      KEYS.four,
      KEYS.five,
      KEYS.six,
      KEYS.cosine,
      KEYS.one,
      KEYS.two,
      KEYS.three,
      KEYS.tangent,
      KEYS.backspace,
      KEYS.pi,
      KEYS.i,
      KEYS.sqrt
    ]
  }
};

export const PLOT_LAYOUTS: LayoutMap = {
  arithmetic: {
    baseUnit: 1,
    rows: 5,
    columns: 4,
    keys: [
      KEYS.switch_layout,
      KEYS.lparen,
      KEYS.rparen,
      KEYS.division,
      KEYS.seven,
      KEYS.eight,
      KEYS.nine,
      KEYS.multiplication,
      KEYS.four,
      KEYS.five,
      KEYS.six,
      KEYS.subtraction,
      KEYS.one,
      KEYS.two,
      KEYS.three,
      KEYS.addition,
      KEYS.backspace,
      KEYS.zero,
      KEYS.x,
      labeledKey({ name: "ok" }, "ok")
    ]
  },
  transcendental: {
    baseUnit: 1,
    rows: 5,
    columns: 4,
    keys: [
      KEYS.switch_layout,
      KEYS.answer,
      KEYS.e,
      KEYS.exponent,
      KEYS.seven,
      KEYS.eight,
      KEYS.nine,
      KEYS.sine,
      KEYS.four,
      KEYS.five,
      KEYS.six,
      KEYS.cosine,
      KEYS.one,
      KEYS.two,
      KEYS.three,
      KEYS.tangent,
      KEYS.backspace,
      KEYS.pi,
      KEYS.i,
      KEYS.sqrt
    ]
  }
};

function mapKey(key: string): string {
  switch (key) {
    case "backspace":
    case "delete":
      return "backspace";
    case "enter":
      return "=";
    case "add":
      return "+";
    case "subtract":
      return "-";
    case "multiply":
      return "*";
    case "divide":
      return "/";
    default:
      return key;
  }
}

function applyModifiers(ev: KeyboardEvent, char: Character): Character {
  if (ev.altKey) char.mod = [Modifier.Alt];
  if (ev.ctrlKey) {
    if (!char.mod) char.mod = [];
    char.mod.push(Modifier.Ctrl);
  }
  if (ev.metaKey) {
    if (!char.mod) char.mod = [];
    char.mod.push(Modifier.Meta);
  }
  if (ev.shiftKey) {
    if (!char.mod) char.mod = [];
    char.mod.push(Modifier.Shift);
  }
  return char;
}

export function fromKeyboardEvent(ev: KeyboardEvent): Character | undefined {
  const key = mapKey(ev.key.toLowerCase());
  if (CHARS.hasOwnProperty(key)) {
    const char = CHARS[key];
    return applyModifiers(ev, char);
  }
  return { name: key };
}

export function useKeyboardLayout(layout: string, layouts = stdLayouts) {
  const lays = reactive(layouts);
  const curLayoutName = ref(layout);

  const curLayout = computed(() => lays[curLayoutName.value]);

  const setLayout = (name: string) => (curLayoutName.value = name);

  const registerLayout = (name: string, layout: Layout) =>
    (lays[name] = layout);

  return {
    layout: curLayout,
    setLayout,
    registerLayout
  };
}
