export function format(complex: Float64Array): string {
  let out = "";
  if (complex[0] === 0) {
    out += complex[1];

    if (complex[1] !== 0) {
      out += "i";
    }
    return out;
  }

  if (isNaN(complex[0]) && !isNaN(complex[1])) {
    out += complex[1] + "i";
    return out;
  }
  out += complex[0];

  if (complex[1] !== 0 && !isNaN(complex[1])) {
    if (complex[1] > 0) {
      out += "+";
    }
    out += complex[1] + "i";
  }
  return out;
}
