import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

import Basic from "@/views/Basic.vue";

const routes: RouteRecordRaw[] = [
  {
    path: "/",
    name: "Basic",
    component: Basic
  },
  {
    path: "/plot",
    name: "Plot",
    component: () => import("@/views/Plot.vue"),
    props: route => ({ d: parseInt(route.query.d as string) })
  },
  {
    path: "/settings",
    name: "Settings",
    component: () => import("@/views/Settings.vue")
  },
  {
    path: "/releases",
    name: "Releases",
    component: () => import("@/views/Releases.vue")
  }
];

const router = createRouter({
  history: createWebHistory(process.env.VUE_APP_DEPLOY_URL || "/"),
  routes
});

export default router;
