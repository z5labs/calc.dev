import { register } from "register-service-worker";
import { bus } from "./event";
import SWUpdateEvent from "./SWUpdateEvent";

let captureException: (e: Error) => void;

import("@sentry/browser").then(mod => {
  captureException = mod.captureException;
});

if (process.env.NODE_ENV === "production") {
  register(`${process.env.BASE_URL}service-worker.js`, {
    updated: registration =>
      bus.emit("sw-updated", new SWUpdateEvent(registration)),
    error: err => captureException(err)
  });
}
