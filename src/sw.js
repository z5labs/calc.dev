addEventListener("message", ev => {
  const replyPort = ev.ports[0];
  const message = ev.data;
  if (replyPort && message && message.type === "skip-waiting") {
    ev.waitUntil(
      self.skipWaiting().then(
        () => replyPort.postMessage({ error: null }),
        error => replyPort.postMessage({ error })
      )
    );
  }
});

workbox.core.clientsClaim();

// The precaching code provided by Workbox.
self.__precacheManifest = [].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});
