export type Float = { tag: "float"; value: string };
export type Variable = { tag: "var"; value: string };
export type Const = { tag: "const"; value: string };

export type Unary = { tag: "unary"; value: string };
export type Binary = { tag: "binary"; value: string };
export type Op = Unary | Binary;

export type Paren = { tag: "paren"; value: string };

export type Token = Const | Float | Variable | Op | Paren;

export function match(
  t: Token,
  c: (_: Const) => void,
  f: (_: Float) => void,
  v: (_: Variable) => void,
  o: (_: Op) => void,
  p: (_: Paren) => void
) {
  switch (t.tag) {
    case "const":
      c(t);
      break;
    case "float":
      f(t);
      break;
    case "var":
      v(t);
      break;
    case "unary":
      o(t);
      break;
    case "binary":
      o(t);
      break;
    case "paren":
      p(t);
      break;
  }
}

export function isBinary(t: Token): t is Binary {
  return t.tag === "binary";
}

export function isUnary(t: Token): t is Unary {
  return t.tag === "unary";
}

export function isOp(t: Token): t is Op {
  return t.tag === "unary" || t.tag === "binary";
}

export function isConst(t: Token): t is Const {
  return t.tag === "const";
}

export function isFloat(t: Token): t is Float {
  return t.tag === "float";
}

export function isVar(t: Token): t is Variable {
  return t.tag === "var";
}

export function float(val: string): Float {
  return { tag: "float", value: val };
}

export function constant(val: string): Const {
  return { tag: "const", value: val };
}

export function variable(val: string): Variable {
  return { tag: "var", value: val };
}

export function paren(val: string): Paren {
  return { tag: "paren", value: val };
}

export function unary(val: string): Unary {
  return { tag: "unary", value: val };
}

export function binary(val: string): Binary {
  return { tag: "binary", value: val };
}
