import { defineAsyncComponent, markRaw } from "vue";
import { createStore } from "vuex";
import { persist } from "@/store/persist";

export interface Units {
  angle: "degree" | "radian";
}

export interface ViewSettings {
  name: string;
  settings: any;
}

export interface Settings {
  views: ViewSettings[];
}

export interface RootState {
  units: Units;
  settings: Settings;
}

export default createStore({
  state: {
    units: {
      angle: "degree"
    }
  },
  mutations: {
    changeAngleUnits(state, unit) {
      state.units.angle = unit;
    }
  },
  actions: {},
  modules: {},
  plugins: [persist({})]
});
