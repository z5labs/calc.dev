import { Store, Plugin, MutationPayload } from "vuex";
import Stream from "ts-stream";

export interface Mutation<S> {
  mutation: MutationPayload;
  state: S;
}

export type MutationStream<S> = Stream<Mutation<S>>;

export interface Config {
  transformer?: <S, T>(payload: Mutation<S>) => T;
}

export function persist<S>(cfg: Config): Plugin<S> {
  return (store: Store<S>) => {
    const stream = new Stream<Mutation<S>>();

    store.subscribe(
      async (mutation, state) => await stream.write({ mutation, state })
    );

    stream
      .map(payload => (cfg.transformer ? cfg.transformer(payload) : payload))
      .map(payload => payload.state)
      .forEach(state =>
        Object.entries(state)
          .map(([k, v]) => [k, JSON.stringify(v)])
          .forEach(([k, v]) => localStorage.setItem(k, v))
      );
  };
}
