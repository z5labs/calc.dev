import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "@/router";
import store from "@/store";

import { Vue as VueIntegration } from "@sentry/integrations";
import { directives } from "@/directives";

const app = createApp(App);

// Register global directives
Object.entries(directives).forEach(([name, v]) => app.directive(name, v));

// Only integrate Sentry into production builds
// if (process.env.NODE_ENV === "production") {
//   import("@sentry/browser").then(mod =>
//     mod.init({
//       dsn: process.env.VUE_APP_SENTRY_DSN,
//       integrations: [
//         new VueIntegration({ app, attachProps: true, logErrors: true })
//       ],
//       release: process.env.VUE_APP_VERSION
//     })
//   );
// }

app
  .use(store)
  .use(router)
  .mount("#app");
