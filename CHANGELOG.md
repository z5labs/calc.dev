# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.8.0-rc.5](https://gitlab.com/z5labs/calc.dev/compare/v0.8.0-rc.4...v0.8.0-rc.5) (2021-09-05)


### Features

* **view/plot:** constrain plot range ([9016548](https://gitlab.com/z5labs/calc.dev/commit/9016548930a3056afd7729e5e138c33f8d8a0e06))


### Bug Fixes

* **view/plot:** actually use domain settings when generating plot ([da572f8](https://gitlab.com/z5labs/calc.dev/commit/da572f8cfa9204e999a1261428e1c9fb70d9224f))

### [0.8.0-rc.4](https://gitlab.com/z5labs/calc.dev/compare/v0.8.0-rc.3...v0.8.0-rc.4) (2021-08-30)


### Features

* **@calc/image:** svg now supports various attributes ([4ba0426](https://gitlab.com/z5labs/calc.dev/commit/4ba0426c643eff335c0b89daff774e6d18f1e08f))
* **settings/plot:** can now configure the domain used in the plot ([6d60269](https://gitlab.com/z5labs/calc.dev/commit/6d602693ce26b2765f78e843e747d42efd1822ff))


### Bug Fixes

* **notifications:** unify api and make sure actions are visible ([b0f8991](https://gitlab.com/z5labs/calc.dev/commit/b0f8991994f1ff891b6281e61721740e1c63eb83))
* **view/plot:** keyboard no longer overflows on smaller screens ([176eeb5](https://gitlab.com/z5labs/calc.dev/commit/176eeb5239d03564840020db07e41305bbbea565))

### [0.8.0-rc.3](https://gitlab.com/z5labs/calc.dev/compare/v0.8.0-rc.2...v0.8.0-rc.3) (2021-08-28)


### Features

* **@calc/image:** outline crate along with SVG api ([c982f04](https://gitlab.com/z5labs/calc.dev/commit/c982f04f0a2edec412d167f74bed2b8185f7c47a))
* **@calc/plot:** impl basic svg renderer ([93347c4](https://gitlab.com/z5labs/calc.dev/commit/93347c4b26aa423923e35fc3cc22c6c2d407255d))
* **settings:** persist to improve user experience ([b10b1fa](https://gitlab.com/z5labs/calc.dev/commit/b10b1faffe66d07f950d48ebedf8177468cf89a5))


### Bug Fixes

* **keyboard:** ignore unsupported characters ([f0af07b](https://gitlab.com/z5labs/calc.dev/commit/f0af07bdc31d37057674b96f7b104388d7dbe15d))

### [0.8.0-rc.2](https://gitlab.com/z5labs/calc.dev/compare/v0.8.0-rc.1...v0.8.0-rc.2) (2021-02-27)


### Features

* **comp/keyboard:** create grid based keyboard that supports flexible layouts ([554bf3f](https://gitlab.com/z5labs/calc.dev/commit/554bf3f3a674bdcc7b9791e2dce34427725c9aba))


### Bug Fixes

* **transitions:** small bug because vue 3 uses different names than vue 2 ([ec03486](https://gitlab.com/z5labs/calc.dev/commit/ec03486f96a74382048d0dce3b0082b5921eb9cb))
* **wasm:** load wasm with renderless component to provide a more consistent experience ([ebf21c6](https://gitlab.com/z5labs/calc.dev/commit/ebf21c6b4f380f5d665dea859f5747ea177f9bf4))

### [0.8.0-rc.1](https://gitlab.com/z5labs/calc.dev/compare/v0.7.7...v0.8.0-rc.1) (2021-01-24)


### Features

* **cli:** the functionality of calc.dev is now usable in the terminal ([eeacab0](https://gitlab.com/z5labs/calc.dev/commit/eeacab04b58639c8780adcdfd5bfa98e3cf8907b))
* **cmd/compute:** rough out mean subcommand ([e795b2b](https://gitlab.com/z5labs/calc.dev/commit/e795b2b4b20e9a0dcb5d5236be9d2d766c492fd8))
* **cmd/compute:** support subcommands ([107b09d](https://gitlab.com/z5labs/calc.dev/commit/107b09d8c29e11eea8acdf017e7cf9ad710ee98f))
* **runtime:** upgrade to Vue 3 for improved performance and smaller app size ([2cac228](https://gitlab.com/z5labs/calc.dev/commit/2cac2287506f39e72cf1cfe69a140ae5a01327d1))

### [0.7.7](https://gitlab.com/z5labs/calc.dev/compare/v0.7.6...v0.7.7) (2020-09-03)


### Bug Fixes

* **view/settings:** text can now be read fully on smaller screens instead of being cut short ([98e3d81](https://gitlab.com/z5labs/calc.dev/commit/98e3d81772ff50823b57b0f42aca15b4b748af66))

### [0.7.6](https://gitlab.com/z5labs/calc.dev/compare/v0.7.5...v0.7.6) (2020-09-02)


### Bug Fixes

* **comp/general-info:** inline arrow svg icon ([e008033](https://gitlab.com/z5labs/calc.dev/commit/e0080337eac05fd8e20b2555879561e3b08d1a52))

### [0.7.5](https://gitlab.com/z5labs/calc.dev/compare/v0.7.4...v0.7.5) (2020-09-01)


### Features

* **view/releases:** user can find the release notes for every version under the settings ([43951d5](https://gitlab.com/z5labs/calc.dev/commit/43951d55b8be98a2b8ff4f4c2210a1c5bff677c1))

### [0.7.4](https://gitlab.com/z5labs/calc.dev/compare/v0.7.3...v0.7.4) (2020-08-29)


### Bug Fixes

* **comp/expr:** don't affect the expression the user originally typed in ([773a5ef](https://gitlab.com/z5labs/calc.dev/commit/773a5ef4280831dbe208f6ecd6db3e5231488f22))
* **view/basic:** wrap result onto next line ([268c706](https://gitlab.com/z5labs/calc.dev/commit/268c706bb92ddf10af6fd0d7eb35589ed8c8c443))

### [0.7.3](https://gitlab.com/z5labs/calc.dev/compare/v0.7.2...v0.7.3) (2020-08-26)


### Features

* **ios/splash-screen:** add dark mode support for splash screens ([0137af2](https://gitlab.com/z5labs/calc.dev/commit/0137af2273a799faf8f19b1e3d246772ccc5b2ef))


### Bug Fixes

* **material/icon-button:** remove focus outline ([e93345d](https://gitlab.com/z5labs/calc.dev/commit/e93345dfa8044cfff096e07fb3265377fdcb44c1))

### [0.7.2](https://gitlab.com/z5labs/calc.dev/compare/v0.7.1...v0.7.2) (2020-08-26)


### Bug Fixes

* **image/android-chrome:** add non maskable ones ([d961536](https://gitlab.com/z5labs/calc.dev/commit/d96153655aff0c24f9e9d188ec2174ebffe30335))

### [0.7.1](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0...v0.7.1) (2020-08-26)


### Bug Fixes

* **image/maskable:** rename android chrome maskable images to proper format ([c378936](https://gitlab.com/z5labs/calc.dev/commit/c3789361b5e9a8bd9c8716ac2658123724aaa390))

## [0.7.0](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.25...v0.7.0) (2020-08-26)


### Features

* **app/social:** add open graph tags for better interaction with social media ([584f95b](https://gitlab.com/z5labs/calc.dev/commit/584f95b5d20d4499acea060986e9e9a55a31a744))
* **view/basic:** auto fill in ans when the user tries to start an expression with a binary operator ([6e7d33e](https://gitlab.com/z5labs/calc.dev/commit/6e7d33e91ddd38e71336f1a1d4d79ba9b6973214))


### Bug Fixes

* **app/images:** all images and icons are now consistent across all platforms ([96bc1a7](https://gitlab.com/z5labs/calc.dev/commit/96bc1a70edc39451e072511983a42a4e7c15bdd0))
* **changelog:** broken links refering to old url for project ([76fd55e](https://gitlab.com/z5labs/calc.dev/commit/76fd55e7f232a8b957c0e3231144c2fd8175c0de))

## [0.7.0-rc.25](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.24...v0.7.0-rc.25) (2020-08-25)


### Features

* **app/theme:** use css variables to configure theme colors ([86f884f](https://gitlab.com/z5labs/calc.dev/commit/86f884fefa7ee2f6d41c28a844080e6198cab68e))


### Bug Fixes

* **app/nav-drawer:** rename to calc.dev ([64fc472](https://gitlab.com/z5labs/calc.dev/commit/64fc472b93170363f191c820ab7916d111978bb8))
* **app/theme:** set manifest theme color to black instead of white ([953d07c](https://gitlab.com/z5labs/calc.dev/commit/953d07c8129b14aa87d79499bc408a119001f6f0))
* **share:** update link and name to calc.dev ([bf1f9e9](https://gitlab.com/z5labs/calc.dev/commit/bf1f9e94107c738c861476ea4dcdf6b7cd52c7e9))

## [0.7.0-rc.24](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.23...v0.7.0-rc.24) (2020-08-24)


### Bug Fixes

* **name:** forgot to change everything to calc.dev ([0f866e1](https://gitlab.com/z5labs/calc.dev/commit/0f866e14409a9e6741da4e7f3735af8b627371b7))

## [0.7.0-rc.23](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.22...v0.7.0-rc.23) (2020-08-24)

## [0.7.0-rc.22](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.21...v0.7.0-rc.22) (2020-08-24)

## [0.7.0-rc.21](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.20...v0.7.0-rc.21) (2020-08-24)


### Bug Fixes

* **build/ci:** link to netlify before deploying ([9d7ce4f](https://gitlab.com/z5labs/calc.dev/commit/9d7ce4f48b026191f692abd89db184990e894ef0))

## [0.7.0-rc.20](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.19...v0.7.0-rc.20) (2020-08-24)


### Bug Fixes

* **banner/icons:** top bar icons should now be visible again on iOS ([299f550](https://gitlab.com/z5labs/calc.dev/commit/299f550d9d36377c12af6a7b1a2e30172ed3c9ad))

## [0.7.0-rc.19](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.18...v0.7.0-rc.19) (2020-08-24)

## [0.7.0-rc.18](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.17...v0.7.0-rc.18) (2020-08-24)

## [0.7.0-rc.17](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.16...v0.7.0-rc.17) (2020-08-22)


### Features

* **comp/expr:** create reusable renderless component for working with expressions ([48fb675](https://gitlab.com/z5labs/calc.dev/commit/48fb6753afc75904a8e27ccb79a79d8b64d4ed04))
* **view/basic:** support result variable named "ans" ([3f9b599](https://gitlab.com/z5labs/calc.dev/commit/3f9b59979ca3d55a8f12ce976a09c2098479889d))


### Bug Fixes

* **comp/install-prompter:** watch display-mode media query during installation ([4eb9f0d](https://gitlab.com/z5labs/calc.dev/commit/4eb9f0d7b8f135c7ea565490ea03c2549df6b946))

## [0.7.0-rc.16](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.15...v0.7.0-rc.16) (2020-08-21)


### Bug Fixes

* **comp/install-prompter:** register beforeinstallprompt listener earlier than mounted ([e024144](https://gitlab.com/z5labs/calc.dev/commit/e02414467664b5e68cf741f3def7e1157878ba50))

## [0.7.0-rc.15](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.14...v0.7.0-rc.15) (2020-08-21)


### Features

* **app/manifest:** add related app for android chrome ([34164fa](https://gitlab.com/z5labs/calc.dev/commit/34164fa3c101ac8ae7f4ac735a10dfbed8020c41))


### Bug Fixes

* **app/install:** only rely on css display mode for checking if app is installed ([e697285](https://gitlab.com/z5labs/calc.dev/commit/e697285b62dba97658d1fccd426d62c2f3550b44))

## [0.7.0-rc.14](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.13...v0.7.0-rc.14) (2020-08-20)


### Features

* **comp/device-info:** created comp for displaying system info ([128c946](https://gitlab.com/z5labs/calc.dev/commit/128c946a90a148f730e5d0a54e5cd5f5693109c3))
* **view/settings:** display device info ([501b716](https://gitlab.com/z5labs/calc.dev/commit/501b716b82a6a11246827eba4abd7cb473e165b9))

## [0.7.0-rc.13](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.12...v0.7.0-rc.13) (2020-08-20)


### Bug Fixes

* **comp/install-prompter:** properly checks if pwa is installed ([b9805b3](https://gitlab.com/z5labs/calc.dev/commit/b9805b3ec37e494d6d96057f2e4be94db776a1e3))

## [0.7.0-rc.12](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.11...v0.7.0-rc.12) (2020-08-20)


### Features

* **app:** update management is now handled by a renderless component ([9d7c8da](https://gitlab.com/z5labs/calc.dev/commit/9d7c8da8bafe7195225fcb4692da3adb2a897ed2))
* **app:** users can now add it to their homescreens even if they dismiss the install banner ([36dac51](https://gitlab.com/z5labs/calc.dev/commit/36dac51a27257b0f6b8f4d7e0db585c5dd0ebffc))
* **comp/input-buttons:** handle keyboard events for digits and basic arithmetic ([3b8c227](https://gitlab.com/z5labs/calc.dev/commit/3b8c2272792bd59c9a7e9ca0f16d4317298262a7))

## [0.7.0-rc.11](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.10...v0.7.0-rc.11) (2020-08-15)


### Bug Fixes

* **images/android:** make fill color white so it's visible in chrome generated splash screen [ci skip] ([9e01528](https://gitlab.com/z5labs/calc.dev/commit/9e01528c975ce82a1285a1351464a736b0656487))

## [0.7.0-rc.10](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.9...v0.7.0-rc.10) (2020-08-15)


### Features

* **view/settings:** settings now have a view where the user can configure them ([89341db](https://gitlab.com/z5labs/calc.dev/commit/89341db7d1875c1008221e19992efc8793849f61))
* **view/settings:** support changing units for angles used in trig functions ([28feeea](https://gitlab.com/z5labs/calc.dev/commit/28feeead3e2423bc0d64c57825f6b2ddf3c57536))

## [0.7.0-rc.9](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.8...v0.7.0-rc.9) (2020-08-12)


### Bug Fixes

* **drawer/navigation:** prevent navigation duplication for already active route ([0ce19e6](https://gitlab.com/z5labs/calc.dev/commit/0ce19e6af34754374283cad8c3d6cd47b978ff46))
* **view/basic:** catch errors and report back to user in output ([b0270d6](https://gitlab.com/z5labs/calc.dev/commit/b0270d6117c631ffa7166fe505588fceb601bd59))

## [0.7.0-rc.8](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.7...v0.7.0-rc.8) (2020-08-12)


### Bug Fixes

* **icon/calculator:** properly handle filling and transparent regions ([7c1e1fd](https://gitlab.com/z5labs/calc.dev/commit/7c1e1fd18971ad3382b02e74d137e987fc110a19))
* **view/basic:** old lines are now reusable again ([7d27e5b](https://gitlab.com/z5labs/calc.dev/commit/7d27e5b8bc343dc65694d614a6d394a29046284b))

## [0.7.0-rc.7](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.6...v0.7.0-rc.7) (2020-08-11)


### Bug Fixes

* **directive/double-tap:** doubletap event now correctly detected ([a3346c1](https://gitlab.com/z5labs/calc.dev/commit/a3346c1212dd47d488ace40d39a3a9474f54918a))

## [0.7.0-rc.6](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.5...v0.7.0-rc.6) (2020-08-10)


### Features

* **view/{basic,plot}:** double tap backspace to clear input ([164c445](https://gitlab.com/z5labs/calc.dev/commit/164c4452f58c4c27bf89c64a04559427e421313a))

## [0.7.0-rc.5](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.4...v0.7.0-rc.5) (2020-08-09)


### Features

* **view/basic:** wrap input to new lines when it gets too long ([daa9146](https://gitlab.com/z5labs/calc.dev/commit/daa91460f706bf5bccb81ab8ce7cf14e79195c3d))

## [0.7.0-rc.4](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.3...v0.7.0-rc.4) (2020-08-09)


### Features

* **ui:** reduce tap recognizer time ([c115566](https://gitlab.com/z5labs/calc.dev/commit/c1155660b2005c63f1477e7e7493f0c3b17c1422))


### Bug Fixes

* **view/basic:** fully support implicit multiplication ([16b9bac](https://gitlab.com/z5labs/calc.dev/commit/16b9bac5eeb043a43d08da3f254e8941e674cfd3))

## [0.7.0-rc.3](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.2...v0.7.0-rc.3) (2020-08-08)


### Features

* **@calc/pexpr:** handle implicit multiplication with unary ops ([db57edd](https://gitlab.com/z5labs/calc.dev/commit/db57edd2510a60a192a3dcdf528eae804b4b4977))
* **@calc/pexpr:** handle implicit variable multiplication ([2f8f6b1](https://gitlab.com/z5labs/calc.dev/commit/2f8f6b1ab6ca603af99317bc387285f56618cc78))
* **@calc/pexpr:** support implicit multiplication between variable and unary op ([0caf9b2](https://gitlab.com/z5labs/calc.dev/commit/0caf9b2b068dbd3e72c0a50cd3c97524d8a6f1e8))
* **buttons:** add btn for the imaginary number i ([541042a](https://gitlab.com/z5labs/calc.dev/commit/541042a8309c8eaa651237fa7251a0335abaec62))
* **pexpr:** support complex numbers ([456123c](https://gitlab.com/z5labs/calc.dev/commit/456123cf4ec10cfe2e7968e73fe5c54e5b6338e3))
* **pexpr:** support implicit leading zero for decimals ([2fd2950](https://gitlab.com/z5labs/calc.dev/commit/2fd29509ee201072451fe58acf736a62189b7914))
* **pexpr/token:** handle negative numbers ([e1f5456](https://gitlab.com/z5labs/calc.dev/commit/e1f5456d97e7d0046fdf776e72cc712de10591bb))
* **view/basic:** add buttons for pi and e ([cf77745](https://gitlab.com/z5labs/calc.dev/commit/cf777459fc9994402d5c04cbd9386cbe8a62c0f9))
* **view/basic:** display imaginary part if result is a complex number ([d289f1a](https://gitlab.com/z5labs/calc.dev/commit/d289f1a65f0c1235a080506d4b1fba84edfebde4))


### Bug Fixes

* **ui:** only show share button for homescreen apps ([29f958e](https://gitlab.com/z5labs/calc.dev/commit/29f958e84f72d29eb7f70e1218bafc72f3d897ec))
* **view/basic:** preserve line history across route changes ([fa2b8da](https://gitlab.com/z5labs/calc.dev/commit/fa2b8da758e3b7635ce3d8869d7d92c83bf2d228))

## [0.7.0-rc.2](https://gitlab.com/z5labs/calc.dev/compare/v0.7.0-rc.1...v0.7.0-rc.2) (2020-05-10)


### Bug Fixes

* **router:** handle non-root base path ([9d90940](https://gitlab.com/z5labs/calc.dev/commit/9d9094053e73e0a4fc2cd248e26d30d643990297))
* **sw:** misspelled arg name ([650fb2d](https://gitlab.com/z5labs/calc.dev/commit/650fb2dec3d5267f6fcb30241b1e753fb574c1c6))

## [0.7.0-rc.1](https://gitlab.com/z5labs/calc.dev/compare/v0.6.0...v0.7.0-rc.1) (2020-05-10)


### Features

* **view/plot:** implemented layout, added wasm api, and extended binary encoding to support variables ([d0cb418](https://gitlab.com/z5labs/calc.dev/commit/d0cb41897dc5060ee96a07323b1886add2eb4fb3))


### Bug Fixes

* **view/plot:** properly handle expressions that end with a variable ([880b98c](https://gitlab.com/z5labs/calc.dev/commit/880b98c6d350dc6feddd0778db2dd8fd48a5b9a1))

## [0.6.0](https://gitlab.com/z5labs/calc.dev/compare/v0.5.4...v0.6.0) (2020-03-17)


### Features

* **polish:** improve performance by nearly 10x ([7ecbb19](https://gitlab.com/z5labs/calc.dev/commit/7ecbb19e97658ffc210b56f3c5b3226e51cee421))


### Bug Fixes

* **app:** update shouldn't appear everytime calc.io is opened on iOS ([ae38592](https://gitlab.com/z5labs/calc.dev/commit/ae385926c94f7eb3d209b33fe787d23d61640dd2))
* **basic:** properly handle decimal values in expressions ([9029952](https://gitlab.com/z5labs/calc.dev/commit/90299527acf3cc51d858a1d610ef98ea7dc6771e))
* **install-prompt:** can now close dialog in iOS by clicking/tapping background ([8ab8d99](https://gitlab.com/z5labs/calc.dev/commit/8ab8d99d5923263a54081578d0e2217825a28e8e))
* **polish:** properly reuse output line keys ([e1a3484](https://gitlab.com/z5labs/calc.dev/commit/e1a3484e372263ab671a0ba02fc172023c7d1be2))

### [0.5.4](https://gitlab.com/z5labs/calc.dev/compare/v0.5.3...v0.5.4) (2020-02-23)

### [0.5.3](https://gitlab.com/z5labs/calc.dev/compare/v0.5.2...v0.5.3) (2020-02-10)


### Features

* **app:** display current app version in nav drawer ([991eca3](https://gitlab.com/z5labs/calc.dev/commit/991eca3ca01d47fe1174f26acdc614bc99a79e2b))

### [0.5.2](https://gitlab.com/z5labs/calc.dev/compare/v0.5.1...v0.5.2) (2020-02-07)


### Features

* **nav-drawer:** change display modes when window size changes ([d0aea50](https://gitlab.com/z5labs/calc.dev/commit/d0aea503018af78ede307aed5fd77c3e63bd82a0))


### Bug Fixes

* **app:** forgot the code that activates the update prompt, whoops! ([932ba13](https://gitlab.com/z5labs/calc.dev/commit/932ba1366dc55e6e1220a239e13b6c6478b9186b))
* **basic:** correctly handle compound expressions e.g. sin(45)^2 ([bf86946](https://gitlab.com/z5labs/calc.dev/commit/bf869462381ced83e496ff0acadb4fc7624bfc3b))

## [0.5.1](https://gitlab.com/z5labs/calc.dev/compare/v0.5.0...v0.5.1) (2020-02-06)


### Features

* **basic**: validate input before execution ([0d1f160](https://gitlab.com/z5labs/calc.dev/commit/0d1f16058f4ddda0ea6338822007c36fe6395c5d))

## [0.5.0](https://gitlab.com/z5labs/calc.dev/compare/v0.4.0...v0.5.0) (2020-02-03)


### Features

* **nav-drawer:** improved desktop experience ([6481517](https://gitlab.com/z5labs/calc.dev/commit/64815172838ead4e8f46a69cd8ae579cd8ee8fb7))
* **app:** add navigation menu drawer ([1e66126](https://gitlab.com/z5labs/calc.dev/commit/1e6612612de32d57d7ae5f2bd54a9f3671ba1b7d))
* **app:** add share button ([def1b2b](https://gitlab.com/z5labs/calc.dev/commit/def1b2b41804505fbc64106e03c4ff87a4026f4c))

## [0.4.0](https://gitlab.com/z5labs/calc.dev/compare/v0.3.1...v0.4.0) (2020-01-27)


### Features

* **app:** added splash screen for ios ([d1bf2f5](https://gitlab.com/z5labs/calc.dev/commit/d1bf2f5ed8a4e9fe10951ad1e75965bbc2d50202))
* **app:** prompt user to refresh on update found ([33addc0](https://gitlab.com/z5labs/calc.dev/commit/33addc0098c4329b871158f1d3b3aebdb3272691))


### Bug Fixes

* **mat-dialog:** displays correctly on mobile screens now ([3831630](https://gitlab.com/z5labs/calc.dev/commit/3831630ff2844c37aacd0af45e41e51d32044629))
* **comp/install-prompt:** prompt no longer shows on ios after its installed ([6a20977](https://gitlab.com/z5labs/calc.dev/commit/6a2097713eaeaab14a44610fac49a87bc958e56b))

### [0.3.1](https://gitlab.com/z5labs/calc.dev/compare/v0.3.0...v0.3.1) (2020-01-24)

### [0.3.0](https://gitlab.com/z5labs/calc.dev/compare/v0.2.5...v0.3.0) (2020-01-24)


### Features

* **install-prompt:** integrate mat-button ([b70b37e](https://gitlab.com/z5labs/calc.dev/commit/b70b37eb039ea1048b3d340a1555e855ee34cbfd))
* **mat-button:** convert input buttons to mat-buttons ([031494c](https://gitlab.com/z5labs/calc.dev/commit/031494cfb98bd17454608c65451f036f5017db23))
* **mat-button:** implemented material design button ([92ff878](https://gitlab.com/z5labs/calc.dev/commit/92ff87866e90f5a6f5e23cbb5c5e49fafb57a261))
* **app:** prompt users to install calc.io ([0d9a3c2](https://gitlab.com/z5labs/calc.dev/commit/0d9a3c2183ae60e1289b1b242c4e4fb8efa4f049))


### Bug Fixes

* **install-prompt:** detect ios browser name vs safari ([3459827](https://gitlab.com/z5labs/calc.dev/commit/3459827f24e0c0919421c3dbd461e62cf5ab8592))

### [0.2.5](https://gitlab.com/z5labs/calc.dev/compare/v0.2.4...v0.2.5) (2019-12-20)


### Features

* **app:** error reporting and tracking with sentry.io ([6d3995a](https://gitlab.com/z5labs/calc.dev/commit/6d3995a138ed90e6c010217dc8bc1ec5f1be7868))
* **app:** generate CHANGELOG.md ([4904654](https://gitlab.com/z5labs/calc.dev/commit/4904654a26308b4bc9905744a477a6d1e059cc95))

### [0.2.4](https://gitlab.com/z5labs/calc.dev/compare/v0.2.3...v0.2.4) (2019-12-13)


### Features

* **basic/output:** mobile first scroll strategy ([#12](https://gitlab.com/z5labs/calc.dev/issues/#12)) ([9b68775](https://gitlab.com/z5labs/calc.dev/commit/9b687754ffc16c5881d9ea75bc568eb0e295dee8))
* **basic/input:** added key listeners to improve desktop ([#12](https://gitlab.com/z5labs/calc.dev/issues/#12)) ([b169c03](https://gitlab.com/z5labs/calc.dev/commit/b169c03d72389af9e8f7ed6fca5e43d2b37fdd67))


### Bug Fixes

* **fix:** line history input is now copied ([#13](https://gitlab.com/z5labs/calc.dev/issues/#13)) ([bd3f9db](https://gitlab.com/z5labs/calc.dev/commit/bd3f9db78227947541db59c6cf409c4c07fe9f0d))

### [0.2.3](https://gitlab.com/z5labs/calc.dev/compare/v0.2.2...v0.2.3) (2019-12-13)


### Features

* **basic/output:** scroll to bottom ([63d5714](https://gitlab.com/z5labs/calc.dev/commit/63d5714c6c9f34e361eac4c72b3c6e388bf4acab))
* **@calc/pexpr:** moved to f64 calculations ([dfbd446](https://gitlab.com/z5labs/calc.dev/commit/dfbd446e00b6285e1fe97619a5f1335247234e96))

### [0.2.2](https://gitlab.com/z5labs/calc.dev/compare/v0.2.1...v0.2.2) (2019-12-12)


### Features

* **basic:** improve input by using watcher ([ea9a854](https://gitlab.com/z5labs/calc.dev/commit/ea9a85466597b1f72b8249f39779e5d058f0ffd7))

### [0.2.1](https://gitlab.com/z5labs/calc.dev/compare/v0.2.0...v0.2.1) (2019-11-25)


### Features

* **app:** added hammerjs to improve mobile perf ([b542ba4](https://gitlab.com/z5labs/calc.dev/commit/b542ba42e1f4b45e62b3fc9f2cfc10bca922d74e))

## [0.2.0](https://gitlab.com/z5labs/calc.dev/compare/v0.1.10...v0.2.0) (2019-10-27)


### Features

* **basic/output:** begun work on turning output into a virtual scroll ([#5](https://gitlab.com/z5labs/calc.dev/issues/#5)) ([6de3160](https://gitlab.com/z5labs/calc.dev/commit/6de31607fffc3f7c0321cbf5ce4d84c6215465c4))
* **@calc/pexpr:** sqrt + exp are now implemented ([20fa508](https://gitlab.com/z5labs/calc.dev/commit/20fa508899507fac86eb137ac0b3380638378f4c))

### [0.1.10](https://gitlab.com/z5labs/calc.dev/compare/v0.1.9...v0.1.10) (2019-10-27)


### Features

* **app:** improve layout for mobile ([c19b207](https://gitlab.com/z5labs/calc.dev/commit/c19b207ec56c976450af4c21aaf2724c0616a84d))

### [0.1.9](https://gitlab.com/z5labs/calc.dev/compare/v0.1.8...v0.1.9) (2019-10-27)


### Bug Fixes

* **fix:** eslint ignore rules ([ede6c64](https://gitlab.com/z5labs/calc.dev/commit/ede6c646df6ede820f1ced87a193033c84a22912))
* **fix:** .gitlab-ci.yml ([5be5434](https://gitlab.com/z5labs/calc.dev/commit/5be5434c550cb9189fc9f3bf5787c864fd2455df))
* **fix:** .gitlab-ci.yml ([0f15e6e](https://gitlab.com/z5labs/calc.dev/commit/0f15e6ec79ad1237e26828ef74654f207a32e20f))
* **fix:** publish scoped packages as public ([6f98089](https://gitlab.com/z5labs/calc.dev/commit/6f980895252a4258d3d1b26e01469a0863ab00cb))

### [0.1.8](https://gitlab.com/z5labs/calc.dev/compare/v0.1.7...v0.1.8) (2019-10-25)


### Bug Fixes

* **fix:** tan edge cases now handled properly ([#8](https://gitlab.com/z5labs/calc.dev/issues/#8)) ([e4cbfb4](https://gitlab.com/z5labs/calc.dev/commit/e4cbfb4a145ca612b647c4350cb0d782c368a3b5))
* **fix:** unary ops now required to be more verbose ([#9](https://gitlab.com/z5labs/calc.dev/issues/#9)) ([1e12c77](https://gitlab.com/z5labs/calc.dev/commit/1e12c77ce7502f4c9ba874f24fbb6e68017088f0))

### [0.1.7](https://gitlab.com/z5labs/calc.dev/compare/v0.1.6...v0.1.7) (2019-10-24)


### Bug Fixes

* **fix:** compound exprs are now handled properly ([#7](https://gitlab.com/z5labs/calc.dev/issues/#7)) ([af82d14](https://gitlab.com/z5labs/calc.dev/commit/af82d143f72d8a63507fb1d03b26e31ee429f823))

### [0.1.6](https://gitlab.com/z5labs/calc.dev/compare/v0.1.5...v0.1.6) (2019-10-24)


### Features

* **comp/input-buttons:** boldened button text ([#6](https://gitlab.com/z5labs/calc.dev/issues/#6)) ([3a07934](https://gitlab.com/z5labs/calc.dev/commit/3a0793430cc5c907c45b12ac075ff20c5806853a))
* **comp/input-buttons:** improved button text ([#6](https://gitlab.com/z5labs/calc.dev/issues/#6)) ([5d8561a](https://gitlab.com/z5labs/calc.dev/commit/5d8561af35f755bdc8968918db6fc15723a8281e))

### Bug Fixes

* **app:** overflow hidden may work ([#2](https://gitlab.com/z5labs/calc.dev/issues/#2)) ([d72b5c5](https://gitlab.com/z5labs/calc.dev/commit/d72b5c597bbfe64920ea3f4c546864da15ed6dae))

### [0.1.5](https://gitlab.com/z5labs/calc.dev/compare/v0.1.4...v0.1.5) (2019-10-24)


### Features

* **basic/output:** output is now scrollable ([#3](https://gitlab.com/z5labs/calc.dev/issues/#3)) ([b0e8a4c](https://gitlab.com/z5labs/calc.dev/commit/b0e8a4cc9195296b1e4928255b6ed528064d8da7))
* **@calc/pexpr:** basic support for trig ([b7d76f0](https://gitlab.com/z5labs/calc.dev/commit/b7d76f0641a8aea0fc1370212f46558cd6a234ac))


### Bug Fixes

* **app:** improved web app-ness on ios ([#2](https://gitlab.com/z5labs/calc.dev/issues/#2)) ([2323aab](https://gitlab.com/z5labs/calc.dev/commit/2323aab65cb142dc4b87f8be189e2241e23dc80b))
* **basic/output:** output line flexbox on webkit/blink ([dd13fd8](https://gitlab.com/z5labs/calc.dev/commit/dd13fd8a5f8fbd4af60dfd48cdedc0b645e92207))

### [0.1.4](https://gitlab.com/z5labs/calc.dev/compare/v0.1.3...v0.1.4) (2019-10-24)


### Bug Fixes

* **fix:** .gitlab-ci.yml ([e1cc241](https://gitlab.com/z5labs/calc.dev/commit/e1cc24106890ee5da77cc96b471045e09b90cf06))

### [0.1.3](https://gitlab.com/z5labs/calc.dev/compare/v0.1.2...v0.1.3) (2019-10-24)


### Bug Fixes

* **fix:** baseUrl is now correct for gitlab pages ([0a9493e](https://gitlab.com/z5labs/calc.dev/commit/0a9493ea9012af66b494a0e588e0379b4486a5ff))

### [0.1.2](https://gitlab.com/z5labs/calc.dev/compare/v0.1.1...v0.1.2) (2019-10-24)


### Bug Fixes

* **fix:** cypress cmd caching ([0808e4b](https://gitlab.com/z5labs/calc.dev/commit/0808e4b9cdca8de7ba5efacca32edebe3c3afddb))

### [0.1.1](https://gitlab.com/z5labs/calc.dev/compare/v0.1.0...v0.1.1) (2019-10-24)


### Features

* **deploy:** configure gitlab pages deployment ([742f178](https://gitlab.com/z5labs/calc.dev/commit/742f1782c3771436ed8aec25087ecff6371ad216))


### Bug Fixes

* **fix:** pages deployment ([934e544](https://gitlab.com/z5labs/calc.dev/commit/934e544241b84aec2b2c10452f4095b0b4fe284a))

## 0.1.0 (2019-10-24)


### Features

* **app:** removed router and simplified calculator changing ([65d95c2](https://gitlab.com/z5labs/calc.dev/commit/65d95c2897ac9f1e49c5dfaeb5f6e1a18a294ad2))
* **basic:** basic arithmetic calculator complete ([757a040](https://gitlab.com/z5labs/calc.dev/commit/757a04080dd1b2d9fd6d0d5c20d3de1e40415c5b))


### Bug Fixes

* **fix:** handles decimal values properly ([19537af](https://gitlab.com/z5labs/calc.dev/commit/19537afd30e8970cfc24f6d0d435dfd4d2366281))
* **fix:** correctly handles multi digit number ([55b9cbd](https://gitlab.com/z5labs/calc.dev/commit/55b9cbd48ba7fa3686bf0d4148fc758f70c005c7))
