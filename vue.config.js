const path = require("path");
const WasmPackPlugin = require("@wasm-tool/wasm-pack-plugin");

process.env.VUE_APP_VERSION = process.env.VERSION || "0.0.0";

module.exports = {
  productionSourceMap: true,
  pwa: {
    name: "calc.dev",
    themeColor: "#000000",
    msTileColor: "#da532c",
    appleMobileWebAppCapable: "yes",
    appleMobileWebAppStatusBarStyle: "black-translucent",
    manifestOptions: {
      background_color: "#000000",
      display: "standalone",
      lang: "en-US",
      orientation: "portrait",
      dir: "ltr",
      description: "A modern graphing calculator for modern devices.",
      categories: [
        "productivity",
        "utilities",
        "education",
        "business",
        "finance"
      ],
      scope: "/",
      related_applications: [
        {
          platform: "webapp",
          url: "https://calc.dev/manifest.json"
        }
      ]
    },
    iconPaths: {
      favicon32: "img/icons/favicon-32x32.png",
      favicon16: "img/icons/favicon-16x16.png",
      appleTouchIcon: "img/icons/apple-touch-icon.png",
      maskIcon: "img/icons/safari-pinned-tab.svg",
      msTileImage: "img/icons/mstile-144x144.png"
    },
    workboxPluginMode: "InjectManifest",
    workboxOptions: {
      swSrc: "./src/sw.js",
      swDest: "service-worker.js"
    }
  },
  chainWebpack: config => {
    if (process.env.NODE_ENV === "production")
      config.devtool("hidden-source-map");

    // Resolve @calc modules locally
    config.resolve.modules.prepend(__dirname);

    // Inline svg images
    config.module
      .rule("vue")
      .use("vue-svg-inline-loader")
      .loader("vue-svg-inline-loader");

    // Compile Rust using wasm-pack
    ["pexpr", "plot"]
      .map(mod => ({
        crateDirectory: path.resolve(__dirname, mod),
        extraArgs: "-s calc --target bundler",
        // The same as the `--out-dir` option for `wasm-pack`
        outDir: path.resolve(__dirname, `@calc/${mod}`),
        outName: mod
      }))
      .forEach(cfg => {
        console.log(cfg);
        config.plugin(cfg.outName).use(WasmPackPlugin, [cfg]);
      });
  }
};
