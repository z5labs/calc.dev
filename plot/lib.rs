#![feature(test)]

extern crate test;

use std::iter::Iterator;

use image::color::Gray;
use image::svg::{Attrs, Color, Stroke, Unit, SVG};
use image::{Image, Img, Point, Rectangle};
use num::complex::Complex;
use pexpr::{eval, Polisher, Token, Tokenizer};

use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
pub struct Settings {
    pub x_min: f64,
    pub x_max: f64,
    pub y_min: f64,
    pub y_max: f64,
    pub step: f64,
}

#[wasm_bindgen]
impl Settings {
    #[wasm_bindgen(constructor)]
    pub fn new(x_min: f64, x_max: f64, y_min: f64, y_max: f64, step: f64) -> Self {
        Self {
            x_min,
            x_max,
            y_min,
            y_max,
            step,
        }
    }
}

struct Domain {
    start: f64,
    end: f64,
    step: f64,
    i: Option<f64>,
}

impl Domain {
    fn new(start: f64, end: f64, step: f64) -> Self {
        Self {
            start,
            end,
            step,
            i: None,
        }
    }
}

impl Iterator for Domain {
    type Item = f64;

    fn next(&mut self) -> Option<Self::Item> {
        if let None = self.i {
            self.i = Some(self.start);
            return Some(self.start);
        }
        let mut v = self.i.unwrap();
        if v > self.end {
            return None;
        }
        v += self.step;
        self.i = Some(v);
        return Some(v);
    }
}

#[wasm_bindgen]
pub fn plot(expr: &str, cfg: Settings) -> String {
    let width = (cfg.x_max - cfg.x_min).abs();
    let height = (cfg.y_max - cfg.y_min).abs();
    let half_w = width / 2.0;
    let half_h = height / 2.0;
    let domain = Domain::new(cfg.x_min, cfg.x_max, cfg.step);
    let toks: Vec<Token> = Polisher::new(Tokenizer::new(expr)).collect();
    let plot: Vec<(f64, f64)> = domain
        .map(|z| {
            let w = eval(toks.clone().into_iter(), Complex::new(z, 0.0));
            (z, w.re)
        })
        .filter(|(_, y)| y > &-half_h && y < &half_h)
        .collect();
    let renderer = Continuous::new(-half_w, -half_h, width, height);
    format!("{}", renderer.render(plot.into_iter()))
}

pub trait Renderer<X, Y, I: Img> {
    fn render(&self, plot: impl Iterator<Item = (X, Y)>) -> I;
}

/// Generate discrete point based plots.
#[derive(Debug, Copy, Clone)]
struct Discrete;

impl Discrete {}

impl Renderer<i64, i64, Image<Gray>> for Discrete {
    fn render(&self, plot: impl Iterator<Item = (i64, i64)>) -> Image<Gray> {
        let mut vals: Vec<(i64, i64)> = plot.collect();
        let (min_x, _) = vals[0];
        let (max_x, _) = vals[vals.len() - 1];

        vals.sort_by(|a, b| b.1.cmp(&a.1));
        let (_, min_y) = vals[vals.len() - 1];
        let (_, max_y) = vals[0];

        let width = (max_x - min_x).abs();
        let height = (max_y - min_y).abs();

        let bounds = Rectangle::new(Point::new(0, 0), Point::new(width, height));
        let mut img: Image<Gray> = Image::new(bounds);

        let mut cur = vals.pop().unwrap();
        for n in 0..width * height {
            let x = (n % width) as i64;
            let y = (n / width) as i64;

            let mut color = 255;
            if x == cur.0 && y == cur.1 {
                color = 0;
                cur = vals.pop().unwrap();
            }

            img.set_pixel(n as usize, Gray(color));
        }

        img
    }
}

/// Generate plots made up of continuous curves.
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Continuous {
    view_box: Rectangle,
}

impl Continuous {
    pub fn new(x: f64, y: f64, width: f64, height: f64) -> Self {
        Self {
            view_box: Rectangle::new(
                Point::new(x as i64, y as i64),
                Point::new(width as i64, height as i64),
            ),
        }
    }
}

macro_rules! render_continuous {
    ($x:ty,$y:ty) => {
        impl<'a> Renderer<$x, $y, SVG<'a>> for Continuous {
            fn render(&self, plot: impl Iterator<Item = ($x, $y)>) -> SVG<'a> {
                SVG::new(self.view_box)
                    .attrs(
                        Attrs::with_capacity(3)
                            .width(Unit::Percent(100.0))
                            .height(Unit::Percent(100.0))
                            .fill(Color::None),
                    )
                    .polyline(
                        plot.map(|(x, y)| (x as f32, -y as f32)).collect(),
                        Some(Attrs::new().stroke(Stroke::new(Color::Named("black")))),
                    )
            }
        }
    };
}

render_continuous!(i8, i8);
render_continuous!(i16, i16);
render_continuous!(i32, i32);
render_continuous!(i64, i64);
render_continuous!(f32, f32);
render_continuous!(f64, f64);

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[bench]
    fn bench_plot_linear_tiny_step(b: &mut Bencher) {
        let expr = "(x)";

        b.iter(|| plot(expr, Settings::new(-10.0, 10.0, -10.0, 10.0, 0.01)));
    }

    #[bench]
    fn bench_plot_linear_small_step(b: &mut Bencher) {
        let expr = "(x)";

        b.iter(|| plot(expr, Settings::new(-10.0, 10.0, -10.0, 10.0, 0.1)));
    }

    #[bench]
    fn bench_plot_linear_large_step(b: &mut Bencher) {
        let expr = "(x)";

        b.iter(|| plot(expr, Settings::new(-10.0, 10.0, -10.0, 10.0, 1.0)));
    }

    #[bench]
    fn bench_plot_poly_tiny_step(b: &mut Bencher) {
        let expr = "(x^3)";

        b.iter(|| plot(expr, Settings::new(-10.0, 10.0, -10.0, 10.0, 0.01)));
    }

    #[bench]
    fn bench_plot_poly_small_step(b: &mut Bencher) {
        let expr = "(x^3)";

        b.iter(|| plot(expr, Settings::new(-10.0, 10.0, -10.0, 10.0, 0.1)));
    }

    #[bench]
    fn bench_plot_poly_large_step(b: &mut Bencher) {
        let expr = "(x^3)";

        b.iter(|| plot(expr, Settings::new(-10.0, 10.0, -10.0, 10.0, 1.0)));
    }

    #[bench]
    fn bench_plot_sin_tiny_step(b: &mut Bencher) {
        let expr = "(sin(x))";

        b.iter(|| plot(expr, Settings::new(-10.0, 10.0, -10.0, 10.0, 0.01)));
    }

    #[bench]
    fn bench_plot_sin_small_step(b: &mut Bencher) {
        let expr = "(sin(x))";

        b.iter(|| plot(expr, Settings::new(-10.0, 10.0, -10.0, 10.0, 0.1)));
    }

    #[bench]
    fn bench_plot_sin_large_step(b: &mut Bencher) {
        let expr = "(sin(x))";

        b.iter(|| plot(expr, Settings::new(-10.0, 10.0, -10.0, 10.0, 1.0)));
    }
}
